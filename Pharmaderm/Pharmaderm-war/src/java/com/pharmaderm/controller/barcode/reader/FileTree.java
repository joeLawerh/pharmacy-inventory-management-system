package com.pharmaderm.controller.barcode.reader;

import java.awt.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.tree.*;
import javax.swing.event.*;

/**
 * FileTree class represents file system browser
 *
 * @author j-bos
 */
public class FileTree extends JPanel {

    public final ImageIcon ICON_COMPUTER = new ImageIcon(getClass().getResource("computer.png"));
    public final ImageIcon ICON_DISK = new ImageIcon(getClass().getResource("drive.png"));
    public final ImageIcon ICON_FOLDER = new ImageIcon(getClass().getResource("folder_closed.png"));
    public final ImageIcon ICON_EXPANDEDFOLDER = new ImageIcon(getClass().getResource("folder_opened.png"));
    public final ImageIcon ICON_PICTURE = new ImageIcon(getClass().getResource("picture.png"));

    protected JTree tree;
    protected DefaultTreeModel model;

    protected ArrayList<FileTreeSelectionEventListener> selectionEventListenerList = new ArrayList<FileTreeSelectionEventListener>();

    public FileTree() {
        setLayout(new BorderLayout());

        // Create root node
        DefaultMutableTreeNode top = new DefaultMutableTreeNode(new TreeNodeData(ICON_COMPUTER, null, "Computer"));
        DefaultMutableTreeNode node;

        // Get root file system objects
        File[] roots = File.listRoots();

        for (int k = 0; k < roots.length; k++) {
            node = new DefaultMutableTreeNode(new TreeNodeData(ICON_DISK, null, new FileNode(roots[k])));
            top.add(node);
            node.add(new DefaultMutableTreeNode()); // dummy node for (+)
        }

        model = new DefaultTreeModel(top);
        tree = new JTree(model);

        // Setup tree
        TreeCellRenderer renderer = new IconCellRenderer();
        tree.setCellRenderer(renderer);
        tree.addTreeExpansionListener(new NodeExpansionListener());
        tree.addTreeSelectionListener(new NodeSelectionListener());
        tree.getSelectionModel().setSelectionMode(TreeSelectionModel.SINGLE_TREE_SELECTION);
        tree.setShowsRootHandles(true);
        tree.setEditable(false);

        JScrollPane scrollPane = new JScrollPane();
        scrollPane.getViewport().add(tree);
        add(scrollPane, BorderLayout.CENTER);

        setVisible(true);
    }

    public void addFileTreeSelectionEventListener(FileTreeSelectionEventListener listener) {
        selectionEventListenerList.add(listener);
    }

    public void removeFileTreeSelectionEventListener(FileTreeSelectionEventListener listener) {
        selectionEventListenerList.remove(listener);
    }

    protected void fireFileTreeSelectionEvent(FileTreeSelectionEvent evt) {
        Iterator listeners = selectionEventListenerList.iterator();

        while (listeners.hasNext()) {
            ((FileTreeSelectionEventListener) listeners.next()).fileTreeSelectionEventOccured(evt);
        }
    }

    DefaultMutableTreeNode getTreeNode(TreePath path) {
        return (DefaultMutableTreeNode) (path.getLastPathComponent());
    }

    FileNode getFileNode(DefaultMutableTreeNode node) {
        if (node == null) {
            return null;
        }

        Object obj = node.getUserObject();
        if (obj instanceof TreeNodeData) {
            obj = ((TreeNodeData) obj).getObject();
        }

        if (obj instanceof FileNode) {
            return (FileNode) obj;
        } else {
            return null;
        }
    }

    /**
     * Gets selected File.
     */
    public File getSelectedFile() {
        TreePath selectionPath = tree.getSelectionPath();
        FileNode fileNode = getFileNode((DefaultMutableTreeNode) selectionPath.getLastPathComponent());

        if (fileNode != null) {
            return fileNode.getFile();
        }

        return null;
    }

    /**
     * Expands file system tree to specified path.
     */
    public void expandPath(String path) {
        File file = new File(path);
        ArrayList<File> pathParts = new ArrayList<File>();

        do {
            pathParts.add(0, file);
            file = file.getParentFile();
        } while (file != null);

        DefaultMutableTreeNode rootNode = (DefaultMutableTreeNode) model.getRoot();

        expand(pathParts, rootNode);
    }

    private void expand(ArrayList<File> pathParts, DefaultMutableTreeNode parentNode) {
        tree.expandPath(new TreePath(parentNode.getPath()));

        Enumeration children = parentNode.children();

        while (children.hasMoreElements()) {
            DefaultMutableTreeNode childNode = (DefaultMutableTreeNode) children.nextElement();
            FileNode fileNode = getFileNode(childNode);

            if (fileNode != null && fileNode.getFile().equals(pathParts.get(0))) {
                pathParts.remove(0);

                if (pathParts.size() > 0) {
                    expand(pathParts, childNode);
                } else {
                    TreePath selectionPath = new TreePath(childNode.getPath());
                    tree.setSelectionPath(selectionPath);
                    tree.scrollPathToVisible(selectionPath);
                }

                break;
            }
        }
    }

    class NodeExpansionListener implements TreeExpansionListener {

        @Override
        public void treeExpanded(TreeExpansionEvent event) {
            final DefaultMutableTreeNode node = getTreeNode(event.getPath());
            final FileNode fnode = getFileNode(node);
            if (fnode != null) {
                fnode.expand(node);
            }
        }

        @Override
        public void treeCollapsed(TreeExpansionEvent event) {
        }
    }

    class NodeSelectionListener implements TreeSelectionListener {

        @Override
        public void valueChanged(TreeSelectionEvent event) {
            DefaultMutableTreeNode node = getTreeNode(event.getPath());
            FileNode fnode = getFileNode(node);

            if (fnode.file != null && !fnode.file.isDirectory()) {
                fireFileTreeSelectionEvent(new FileTreeSelectionEvent(this, fnode.getFile()));
            }
        }
    }

    class IconCellRenderer extends DefaultTreeCellRenderer //JLabel implements TreeCellRenderer
    {

        public IconCellRenderer() {
            super();
        }

        @Override
        public Component getTreeCellRendererComponent(JTree tree, Object value, boolean sel, boolean expanded, boolean leaf, int row, boolean hasFocus) {
            super.getTreeCellRendererComponent(tree, value, sel, expanded, leaf, row, hasFocus);

            DefaultMutableTreeNode node = (DefaultMutableTreeNode) value;
            Object obj = node.getUserObject();

            if (obj instanceof TreeNodeData) {
                TreeNodeData idata = (TreeNodeData) obj;

                if (expanded) {
                    setIcon(idata.getExpandedIcon());
                } else {
                    setIcon(idata.getIcon());
                }
            } else {
                setIcon(null);
            }

            return this;
        }
    }

    class TreeNodeData {

        protected Icon icon;
        protected Icon expandedIcon;
        protected Object data;

        public TreeNodeData(Icon icon, Object data) {
            this.icon = icon;
            expandedIcon = null;
            this.data = data;
        }

        public TreeNodeData(Icon icon, Icon expandedIcon, Object data) {
            this.icon = icon;
            this.expandedIcon = expandedIcon;
            this.data = data;
        }

        public Icon getIcon() {
            return icon;
        }

        public Icon getExpandedIcon() {
            return expandedIcon != null ? expandedIcon : icon;
        }

        public Object getObject() {
            return data;
        }

        @Override
        public String toString() {
            return data.toString();
        }
    }

    class FileNode {

        protected File file;

        public FileNode(File file) {
            this.file = file;
        }

        public File getFile() {
            return file;
        }

        @Override
        public String toString() {
            return file.getName().length() > 0 ? file.getName() : file.getPath();
        }

        public boolean expand(DefaultMutableTreeNode parent) {
            DefaultMutableTreeNode child = (DefaultMutableTreeNode) parent.getFirstChild();
            if (child == null) {
                return false;
            }

            parent.removeAllChildren();

            File[] files = listFiles();
            if (files == null) {
                return true;
            }

            Arrays.sort(files, new Comparator<File>() {
                @Override
                public int compare(File f1, File f2) {
                    if (f1.isDirectory() && f2.isDirectory()) {
                        return f1.getName().compareToIgnoreCase(f2.getName());
                    }

                    if (f1.isDirectory() && !f2.isDirectory()) {
                        return -1;
                    }

                    if (!f1.isDirectory() && f2.isDirectory()) {
                        return 1;
                    }

                    return f1.getName().compareToIgnoreCase(f2.getName());
                }
            });

            for (File file : files) {
                FileNode fileNode = new FileNode(file);
                TreeNodeData data = file.isDirectory()
                        ? new TreeNodeData(FileTree.this.ICON_FOLDER, FileTree.this.ICON_EXPANDEDFOLDER, fileNode)
                        : new TreeNodeData(FileTree.this.ICON_PICTURE, fileNode);
                DefaultMutableTreeNode node = new DefaultMutableTreeNode(data);
                parent.add(node);

                if (file.isDirectory()) {
                    node.add(new DefaultMutableTreeNode()); // dummy node for (+)
                }
            }

            model.reload(parent);

            return true;
        }

        public boolean hasSubDirs() {
            File[] files = listFiles();
            if (files == null) {
                return false;
            }

            for (int k = 0; k < files.length; k++) {
                if (files[k].isDirectory()) {
                    return true;
                }
            }

            return false;
        }

        public int compareTo(FileNode toCompare) {
            return file.getName().compareToIgnoreCase(toCompare.file.getName());
        }

        protected File[] listFiles() {
            if (!file.isDirectory()) {
                return null;
            }

            try {
                return file.listFiles(new FileFilter() {
                    @Override
                    public boolean accept(File pathname) {
                        if (pathname.isDirectory()) {
                            return true;
                        }

                        // only images
                        String name = pathname.getName().toLowerCase();
                        if (name.endsWith(".jpg") || name.endsWith(".jpeg") || name.endsWith(".png") || name.endsWith(".bmp") || name.endsWith(".gif")) {
                            return true;
                        }

                        return false;
                    }
                });
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "Error reading directory " + file.getAbsolutePath(), "Warning", JOptionPane.WARNING_MESSAGE);
                return null;
            }
        }
    }
}

interface FileTreeSelectionEventListener extends EventListener {

    public void fileTreeSelectionEventOccured(FileTreeSelectionEvent event);
}

class FileTreeSelectionEvent extends EventObject {

    public File file;

    public FileTreeSelectionEvent(Object sender, File selectedFile) {
        super(sender);

        this.file = selectedFile;
    }
}
