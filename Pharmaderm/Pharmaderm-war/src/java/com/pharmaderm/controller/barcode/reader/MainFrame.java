package com.pharmaderm.controller.barcode.reader;

import com.bytescout.barcodereader.*;
import java.awt.*;
import java.awt.font.FontRenderContext;
import java.awt.font.LineBreakMeasurer;
import java.awt.font.TextLayout;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.AttributedCharacterIterator;
import java.text.AttributedString;
import java.util.EnumSet;
import java.util.prefs.Preferences;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 *
 * @author J-bos
 */
public class MainFrame extends javax.swing.JFrame implements com.pharmaderm.controller.barcode.reader.FileTreeSelectionEventListener {

    private final Reader reader = new Reader(); // BarCode Reader instance
    private File selectedFile = null;
    private String lastUsedDirectory = null;
    private ImagePanel imagePanel = null;
    private FoundBarcode[] foundBarcodes = null;

    private FileTree fileTree;

    public MainFrame() {
        fileTree = new FileTree();

        initComponents();

        this.setTitle("BarCode Reader v" + VersionInfo.version);

        // Setup BarCode Reader
        reader.setRegistrationName("demo");
        reader.setRegistrationKey("demo");

        // Restore preferences 
        Preferences prefs = Preferences.userNodeForPackage(MainFrame.class);

        int x = prefs.getInt("x", 0);
        int y = prefs.getInt("y", 0);
        int width = prefs.getInt("width", 0);
        int height = prefs.getInt("height", 0);

        if (width != 0 && height != 0) {
            this.setBounds(x, y, width, height);
        } else {
            // center window on the desktop
            setLocationRelativeTo(null);
        }

        int dividerLocation = prefs.getInt("dividerLocation", 0);

        if (dividerLocation > 0) {
            jSplitPane1.setDividerLocation(dividerLocation);
        }

        lastUsedDirectory = prefs.get("lastUsedDirectory", null);

        // Create image panel
        imagePanel = new ImagePanel();
        imagePanel.setMinimumSize(new Dimension(100, 100));
        imageViewScrollPane.setViewportView(imagePanel);
        imageViewScrollPane.setWheelScrollingEnabled(true);
        imageViewScrollPane.getVerticalScrollBar().setUnitIncrement(16);

        fileTree.addFileTreeSelectionEventListener(this);

        String lastSelected = prefs.get("lastSelected", null);

        // Expand file system tree to last selected file
        if (lastSelected != null) {
            fileTree.expandPath(lastSelected);
        }
        this.fileTreePanel = fileTree;
    }

    // Search selected image for barcodes
    private void decodeImage() {
        EnumSet<BarcodeType> types = reader.getBarcodeTypesToFind();
        textArea.setText("Searching image for " + types.toString() + " barcodes...\r\n");

        try {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));

            // Search for barcodes
            foundBarcodes = reader.readFromFile(selectedFile.getAbsolutePath());
        } catch (Exception exception) {
            foundBarcodes = null;
            JOptionPane.showMessageDialog(null, exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        } finally {
            setCursor(Cursor.getDefaultCursor());
        }

        updateImagePanel();
        updateFoundBarcodeInfo();
    }

    // Show selected image
    private void updateImagePanel() {
        BufferedImage image = null;

        try {
            image = ImageIO.read(selectedFile);
        } catch (IOException exception) {
            JOptionPane.showMessageDialog(null, exception.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

        // Convert image to ARGB format
        if (image != null && image.getType() != BufferedImage.TYPE_INT_ARGB) {
            BufferedImage convertedImage = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
            Graphics graphics = convertedImage.getGraphics();
            graphics.drawImage(image, 0, 0, null);
            graphics.dispose();
            image = convertedImage;
        }

        imagePanel.setImage(image);
        imageViewScrollPane.revalidate();
        imageViewScrollPane.repaint();
    }

    // Display information about found barcodes
    private void updateFoundBarcodeInfo() {
        if (foundBarcodes != null && foundBarcodes.length > 0) {
            StringBuilder sb = new StringBuilder();

            Image image = imagePanel.getImage();
            Graphics2D graphics = (Graphics2D) image.getGraphics();

            sb.append(String.format("Found %1$d barcodes:\r\n\r\n", foundBarcodes.length));

            for (int i = 0; i < foundBarcodes.length; i++) {
                sb.append(String.valueOf(i + 1));
                sb.append(") ");
                sb.append(foundBarcodes[i].getType());
                sb.append(", Confidence: ");
                sb.append(String.format("%1$.2f", foundBarcodes[i].getConfidence()));
                sb.append(", Rectangle: ");
                final Rectangle barcodeRectangle = foundBarcodes[i].getRectangle();
                sb.append(String.format("[%1$d; %2$d; %3$d; %4$d]\r\n", barcodeRectangle.x, barcodeRectangle.y, barcodeRectangle.width, barcodeRectangle.height));
                sb.append("Value: ");
                sb.append(foundBarcodes[i].getValue().replace((char) 0, '\u25EA').trim());
                sb.append("\r\n\r\n");

                if (foundBarcodes[i].getPage() == 0) {
                    StringBuilder labelStringBuilder = new StringBuilder();
                    labelStringBuilder.append(String.valueOf(i + 1));
                    labelStringBuilder.append(") ");
                    labelStringBuilder.append(foundBarcodes[i].getType());
                    labelStringBuilder.append(": ");
                    labelStringBuilder.append(foundBarcodes[i].getValue().replace((char) 0, '\u25EA').trim());
                    String labelText = labelStringBuilder.toString();

                    // Draw barcode info over the displayed image
                    Rectangle barcodeRect = foundBarcodes[i].getRectangle();
                    Point p = barcodeRect.getLocation();

                    graphics.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                    graphics.setClip(null);

                    graphics.setPaint(new Color(255, 255, 255, 212));
                    graphics.fillRect(barcodeRect.x, barcodeRect.y, barcodeRect.width, barcodeRect.height);
                    graphics.setPaint(Color.red);
                    graphics.drawRect(barcodeRect.x, barcodeRect.y, barcodeRect.width, barcodeRect.height);
                    graphics.setPaint(Color.black);

                    Rectangle clippingRect = new Rectangle(barcodeRect);
                    clippingRect.width += 1;
                    clippingRect.height += 1;
                    graphics.setClip(clippingRect);

                    Font font = getFont();
                    FontRenderContext frc = graphics.getFontRenderContext();
                    AttributedString attributedString = new AttributedString(labelText);
                    AttributedCharacterIterator paragraph = attributedString.getIterator();
                    LineBreakMeasurer measurer = new LineBreakMeasurer(paragraph, frc);
                    float wrappingWidth = barcodeRect.width;

                    while (measurer.getPosition() < labelText.length()) {
                        TextLayout layout = measurer.nextLayout(wrappingWidth);
                        p.y += layout.getAscent();
                        float dx = layout.isLeftToRight() ? 0 : (wrappingWidth - layout.getAdvance());
                        layout.draw(graphics, p.x + dx, p.y);
                        p.y += layout.getDescent() + layout.getLeading();
                    }
                }
            }

            graphics.dispose();

            imageViewScrollPane.revalidate();
            imageViewScrollPane.repaint();

            textArea.append(sb.toString());
        } else {
            textArea.append("No barcodes found.");
        }
    }

    @Override
    public void fileTreeSelectionEventOccured(FileTreeSelectionEvent event) {
        lastUsedDirectory = event.file.getParent();
        selectedFile = event.file;

        this.setTitle("BarCode Reader v" + VersionInfo.version + " - " + selectedFile.getName());

        updateImagePanel();
    }

    // Image viewer panel
    public class ImagePanel extends JPanel {

        private BufferedImage image = null;

        public ImagePanel() {
        }

        @Override
        public Dimension getPreferredSize() {
            if (image != null) {
                return (new Dimension(image.getWidth(this), image.getHeight(this)));
            }

            return new Dimension(0, 0);
        }

        @Override
        public void paint(Graphics g) {
            g.setColor(getBackground());
            Rectangle r = g.getClipBounds();
            g.fillRect(r.x, r.y, r.width, r.height);

            if (image != null) {
                g.drawImage(image, 0, 0, this);
            }
        }

        public BufferedImage getImage() {
            return image;
        }

        public void setImage(BufferedImage image) {
            if (this.image != image) {
                this.image = image;
                repaint();
            }
        }
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {
        java.awt.GridBagConstraints gridBagConstraints;

        jPanel2 = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        btnOpenFile = new javax.swing.JButton();
        jSeparator5 = new javax.swing.JToolBar.Separator();
        btnOptions = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        btnCode39 = new javax.swing.JButton();
        btnTrioptic = new javax.swing.JButton();
        btnCode128 = new javax.swing.JButton();
        btnUPCA = new javax.swing.JButton();
        btnEAN8 = new javax.swing.JButton();
        btnInterleaved2of5 = new javax.swing.JButton();
        btnAll1D = new javax.swing.JButton();
        btnCode39Ext = new javax.swing.JButton();
        btnCodabar = new javax.swing.JButton();
        btnGS1 = new javax.swing.JButton();
        btnUPCE = new javax.swing.JButton();
        btnEAN13 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        btnQRCode = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jSplitPane1 = new javax.swing.JSplitPane();
        fileTreePanel = new FileTree();
        imageViewScrollPane = new javax.swing.JScrollPane();
        textAreaScrollPane = new javax.swing.JScrollPane();
        textArea = new javax.swing.JTextArea();
        jMenuBar1 = new javax.swing.JMenuBar();
        miFile = new javax.swing.JMenu();
        miOpen = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        miExit = new javax.swing.JMenuItem();
        miDecode = new javax.swing.JMenu();
        miDecode1DSpecific = new javax.swing.JMenuItem();
        miDecode1DAll = new javax.swing.JMenuItem();
        jSeparator2 = new javax.swing.JPopupMenu.Separator();
        miDecode2DSpecific = new javax.swing.JMenuItem();
        miDecode2DAll = new javax.swing.JMenuItem();
        jSeparator3 = new javax.swing.JPopupMenu.Separator();
        miDecodeAll = new javax.swing.JMenuItem();
        miTools = new javax.swing.JMenu();
        miOptions = new javax.swing.JMenuItem();
        miHelp = new javax.swing.JMenu();
        miVisitHomePage = new javax.swing.JMenuItem();
        jSeparator4 = new javax.swing.JPopupMenu.Separator();
        miAbout = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setMinimumSize(new java.awt.Dimension(671, 561));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });
        getContentPane().setLayout(new java.awt.BorderLayout(0, 2));

        jPanel2.setMinimumSize(new java.awt.Dimension(671, 106));
        jPanel2.setPreferredSize(new java.awt.Dimension(671, 106));
        jPanel2.setRequestFocusEnabled(false);
        jPanel2.setLayout(new java.awt.GridBagLayout());

        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);

        btnOpenFile.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pharmaderm/controller/barcode/reader/folder_image.png"))); // NOI18N
        btnOpenFile.setText("Open File");
        btnOpenFile.setFocusable(false);
        btnOpenFile.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnOpenFile.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOpenFileActionPerformed(evt);
            }
        });
        jToolBar1.add(btnOpenFile);
        jToolBar1.add(jSeparator5);

        btnOptions.setIcon(new javax.swing.ImageIcon(getClass().getResource("/com/pharmaderm/controller/barcode/reader/cog.png"))); // NOI18N
        btnOptions.setText("Options");
        btnOptions.setFocusable(false);
        btnOptions.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btnOptions.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnOptionsActionPerformed(evt);
            }
        });
        jToolBar1.add(btnOptions);

        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 0;
        gridBagConstraints.gridwidth = java.awt.GridBagConstraints.REMAINDER;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.anchor = java.awt.GridBagConstraints.PAGE_START;
        gridBagConstraints.weighty = 0.5;
        jPanel2.add(jToolBar1, gridBagConstraints);

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel1.setLabelFor(btnCodabar);
        jLabel1.setText("1D Barcodes:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.weightx = 0.02;
        gridBagConstraints.weighty = 0.15;
        gridBagConstraints.insets = new java.awt.Insets(0, 7, 0, 4);
        jPanel2.add(jLabel1, gridBagConstraints);

        btnCode39.setText("Code 39");
        btnCode39.setFocusable(false);
        btnCode39.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCode39ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.03;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnCode39, gridBagConstraints);

        btnTrioptic.setText("Trioptic");
        btnTrioptic.setFocusable(false);
        btnTrioptic.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnTriopticActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.03;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnTrioptic, gridBagConstraints);

        btnCode128.setText("Code 128");
        btnCode128.setFocusable(false);
        btnCode128.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCode128ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.03;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnCode128, gridBagConstraints);

        btnUPCA.setText("UPCA");
        btnUPCA.setFocusable(false);
        btnUPCA.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUPCAActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.03;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnUPCA, gridBagConstraints);

        btnEAN8.setText("EAN-8");
        btnEAN8.setFocusable(false);
        btnEAN8.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEAN8ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.03;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnEAN8, gridBagConstraints);

        btnInterleaved2of5.setText("Interl. 2 of 5");
        btnInterleaved2of5.setFocusable(false);
        btnInterleaved2of5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnInterleaved2of5ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 6;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.03;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnInterleaved2of5, gridBagConstraints);

        btnAll1D.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btnAll1D.setText("All 1D Barcodes");
        btnAll1D.setFocusable(false);
        btnAll1D.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnAll1DActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 7;
        gridBagConstraints.gridy = 1;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.03;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnAll1D, gridBagConstraints);

        btnCode39Ext.setText("Code 39 Ext");
        btnCode39Ext.setFocusable(false);
        btnCode39Ext.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCode39ExtActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnCode39Ext, gridBagConstraints);

        btnCodabar.setText("Codabar");
        btnCodabar.setFocusable(false);
        btnCodabar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnCodabarActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 2;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnCodabar, gridBagConstraints);

        btnGS1.setText("GS1-128");
        btnGS1.setFocusable(false);
        btnGS1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnGS1ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 3;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnGS1, gridBagConstraints);

        btnUPCE.setText("UPCE");
        btnUPCE.setFocusable(false);
        btnUPCE.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnUPCEActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 4;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnUPCE, gridBagConstraints);

        btnEAN13.setText("EAN-13");
        btnEAN13.setFocusable(false);
        btnEAN13.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnEAN13ActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 5;
        gridBagConstraints.gridy = 2;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(btnEAN13, gridBagConstraints);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jLabel2.setLabelFor(btnCodabar);
        jLabel2.setText("2D Barcodes:");
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 0;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.weightx = 0.02;
        gridBagConstraints.weighty = 0.15;
        gridBagConstraints.insets = new java.awt.Insets(4, 7, 0, 4);
        jPanel2.add(jLabel2, gridBagConstraints);

        btnQRCode.setText("QR Code");
        btnQRCode.setFocusable(false);
        btnQRCode.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnQRCodeActionPerformed(evt);
            }
        });
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.gridx = 1;
        gridBagConstraints.gridy = 3;
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weighty = 0.15;
        gridBagConstraints.insets = new java.awt.Insets(4, 0, 0, 0);
        jPanel2.add(btnQRCode, gridBagConstraints);
        gridBagConstraints = new java.awt.GridBagConstraints();
        gridBagConstraints.fill = java.awt.GridBagConstraints.HORIZONTAL;
        gridBagConstraints.weightx = 0.04;
        gridBagConstraints.weighty = 0.15;
        jPanel2.add(jLabel3, gridBagConstraints);

        getContentPane().add(jPanel2, java.awt.BorderLayout.PAGE_START);

        jSplitPane1.setDividerLocation(200);
        jSplitPane1.setLeftComponent(fileTreePanel);

        imageViewScrollPane.setBorder(javax.swing.BorderFactory.createLineBorder(java.awt.Color.gray));
        jSplitPane1.setRightComponent(imageViewScrollPane);

        getContentPane().add(jSplitPane1, java.awt.BorderLayout.CENTER);

        textArea.setColumns(20);
        textArea.setFont(new java.awt.Font("Tahoma", 0, 11)); // NOI18N
        textArea.setRows(8);
        textArea.setMargin(new java.awt.Insets(5, 5, 5, 5));
        textAreaScrollPane.setViewportView(textArea);

        getContentPane().add(textAreaScrollPane, java.awt.BorderLayout.PAGE_END);

        miFile.setText("File");

        miOpen.setText("Open");
        miFile.add(miOpen);
        miFile.add(jSeparator1);

        miExit.setText("Exit");
        miFile.add(miExit);

        jMenuBar1.add(miFile);

        miDecode.setText("Decode");

        miDecode1DSpecific.setText("Decode specific 1D symbology");
        miDecode.add(miDecode1DSpecific);

        miDecode1DAll.setText("Decode all 1D barcodes");
        miDecode.add(miDecode1DAll);
        miDecode.add(jSeparator2);

        miDecode2DSpecific.setText("Decode specific 2D symbology");
        miDecode.add(miDecode2DSpecific);

        miDecode2DAll.setText("Decode all 2D barcodes");
        miDecode.add(miDecode2DAll);
        miDecode.add(jSeparator3);

        miDecodeAll.setText("Decode All (1D and 2D)");
        miDecode.add(miDecodeAll);

        jMenuBar1.add(miDecode);

        miTools.setText("Tools");

        miOptions.setText("Options");
        miTools.add(miOptions);

        jMenuBar1.add(miTools);

        miHelp.setText("Help");

        miVisitHomePage.setText("Visit BarCode Reader Home Page");
        miHelp.add(miVisitHomePage);
        miHelp.add(jSeparator4);

        miAbout.setText("About");
        miHelp.add(miAbout);

        jMenuBar1.add(miHelp);

        setJMenuBar(jMenuBar1);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt)//GEN-FIRST:event_formWindowClosing
    {//GEN-HEADEREND:event_formWindowClosing
        if (this.getExtendedState() == Frame.NORMAL) {
            Preferences prefs = Preferences.userNodeForPackage(MainFrame.class);

            prefs.putInt("x", this.getX());
            prefs.putInt("y", this.getY());
            prefs.putInt("width", this.getWidth());
            prefs.putInt("height", this.getHeight());
            prefs.putInt("dividerLocation", jSplitPane1.getDividerLocation());
            prefs.put("lastUsedDirectory", lastUsedDirectory);

            if (selectedFile != null) {
                prefs.put("lastSelected", selectedFile.getAbsolutePath());
            }
        }
    }//GEN-LAST:event_formWindowClosing

    private void btnCode39ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCode39ActionPerformed
    {//GEN-HEADEREND:event_btnCode39ActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.Code39));
        decodeImage();
    }//GEN-LAST:event_btnCode39ActionPerformed

    private void btnTriopticActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnTriopticActionPerformed
    {//GEN-HEADEREND:event_btnTriopticActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.TriopticCode39));
        decodeImage();
    }//GEN-LAST:event_btnTriopticActionPerformed

    private void btnCode39ExtActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCode39ExtActionPerformed
    {//GEN-HEADEREND:event_btnCode39ExtActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.Code39Ext));
        decodeImage();
    }//GEN-LAST:event_btnCode39ExtActionPerformed

    private void btnCodabarActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCodabarActionPerformed
    {//GEN-HEADEREND:event_btnCodabarActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.Codabar));
        decodeImage();
    }//GEN-LAST:event_btnCodabarActionPerformed

    private void btnCode128ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnCode128ActionPerformed
    {//GEN-HEADEREND:event_btnCode128ActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.Code128));
        decodeImage();
    }//GEN-LAST:event_btnCode128ActionPerformed

    private void btnGS1ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnGS1ActionPerformed
    {//GEN-HEADEREND:event_btnGS1ActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.GS1));
        decodeImage();
    }//GEN-LAST:event_btnGS1ActionPerformed

    private void btnUPCAActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnUPCAActionPerformed
    {//GEN-HEADEREND:event_btnUPCAActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.UPCA));
        decodeImage();
    }//GEN-LAST:event_btnUPCAActionPerformed

    private void btnUPCEActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnUPCEActionPerformed
    {//GEN-HEADEREND:event_btnUPCEActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.UPCE));
        decodeImage();
    }//GEN-LAST:event_btnUPCEActionPerformed

    private void btnEAN8ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnEAN8ActionPerformed
    {//GEN-HEADEREND:event_btnEAN8ActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.EAN8));
        decodeImage();
    }//GEN-LAST:event_btnEAN8ActionPerformed

    private void btnEAN13ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnEAN13ActionPerformed
    {//GEN-HEADEREND:event_btnEAN13ActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.EAN13));
        decodeImage();
    }//GEN-LAST:event_btnEAN13ActionPerformed

    private void btnInterleaved2of5ActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnInterleaved2of5ActionPerformed
    {//GEN-HEADEREND:event_btnInterleaved2of5ActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.Interleaved2of5));
        decodeImage();
    }//GEN-LAST:event_btnInterleaved2of5ActionPerformed

    private void btnAll1DActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnAll1DActionPerformed
    {//GEN-HEADEREND:event_btnAll1DActionPerformed
        reader.setBarcodeTypesToFind(BarcodeType.getAll1D());
        decodeImage();
    }//GEN-LAST:event_btnAll1DActionPerformed

    private void btnQRCodeActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnQRCodeActionPerformed
    {//GEN-HEADEREND:event_btnQRCodeActionPerformed
        reader.setBarcodeTypesToFind(EnumSet.of(BarcodeType.QRCode));
        decodeImage();
    }//GEN-LAST:event_btnQRCodeActionPerformed

    private void btnOpenFileActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnOpenFileActionPerformed
    {//GEN-HEADEREND:event_btnOpenFileActionPerformed
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileFilter(new FileNameExtensionFilter("Image files", "png", "jpg", "jpeg", "bmp", "gif"));

        if (lastUsedDirectory != null) {
            fileChooser.setCurrentDirectory(new File(lastUsedDirectory));
        }

        int returnVal = fileChooser.showOpenDialog(this);

        if (returnVal == JFileChooser.APPROVE_OPTION) {
            lastUsedDirectory = fileChooser.getCurrentDirectory().getAbsolutePath();
            selectedFile = fileChooser.getSelectedFile();

            this.setTitle("BarCode Reader v" + VersionInfo.version + " - " + selectedFile.getName());

            updateImagePanel();
        }
    }//GEN-LAST:event_btnOpenFileActionPerformed

    private void btnOptionsActionPerformed(java.awt.event.ActionEvent evt)//GEN-FIRST:event_btnOptionsActionPerformed
    {//GEN-HEADEREND:event_btnOptionsActionPerformed
        Options options = new Options(this, reader);
        options.setVisible(true);
    }//GEN-LAST:event_btnOptionsActionPerformed

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btnAll1D;
    private javax.swing.JButton btnCodabar;
    private javax.swing.JButton btnCode128;
    private javax.swing.JButton btnCode39;
    private javax.swing.JButton btnCode39Ext;
    private javax.swing.JButton btnEAN13;
    private javax.swing.JButton btnEAN8;
    private javax.swing.JButton btnGS1;
    private javax.swing.JButton btnInterleaved2of5;
    private javax.swing.JButton btnOpenFile;
    private javax.swing.JButton btnOptions;
    private javax.swing.JButton btnQRCode;
    private javax.swing.JButton btnTrioptic;
    private javax.swing.JButton btnUPCA;
    private javax.swing.JButton btnUPCE;
    private javax.swing.JPanel fileTreePanel;
    private javax.swing.JScrollPane imageViewScrollPane;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JPopupMenu.Separator jSeparator2;
    private javax.swing.JPopupMenu.Separator jSeparator3;
    private javax.swing.JPopupMenu.Separator jSeparator4;
    private javax.swing.JToolBar.Separator jSeparator5;
    private javax.swing.JSplitPane jSplitPane1;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JMenuItem miAbout;
    private javax.swing.JMenu miDecode;
    private javax.swing.JMenuItem miDecode1DAll;
    private javax.swing.JMenuItem miDecode1DSpecific;
    private javax.swing.JMenuItem miDecode2DAll;
    private javax.swing.JMenuItem miDecode2DSpecific;
    private javax.swing.JMenuItem miDecodeAll;
    private javax.swing.JMenuItem miExit;
    private javax.swing.JMenu miFile;
    private javax.swing.JMenu miHelp;
    private javax.swing.JMenuItem miOpen;
    private javax.swing.JMenuItem miOptions;
    private javax.swing.JMenu miTools;
    private javax.swing.JMenuItem miVisitHomePage;
    private javax.swing.JTextArea textArea;
    private javax.swing.JScrollPane textAreaScrollPane;
    // End of variables declaration//GEN-END:variables
}
