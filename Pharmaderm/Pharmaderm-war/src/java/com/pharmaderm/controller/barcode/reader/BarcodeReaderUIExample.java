package com.pharmaderm.controller.barcode.reader;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.UIManager;
//Using directory file:/C:/Users/Educator/Documents/NetBeansProjects/ntj/build/classes/image/
//Image url is file:/C:/Users/Educator/Documents/NetBeansProjects/ntj/build/classes/image/Text.jpg
public class BarcodeReaderUIExample {

    public static void main(String[] args) {

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception ex) {
            Logger.getLogger(BarcodeReaderUIExample.class.getName()).log(Level.SEVERE, null, ex);
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new MainFrame().setVisible(true);
            }
        });
    }
}
