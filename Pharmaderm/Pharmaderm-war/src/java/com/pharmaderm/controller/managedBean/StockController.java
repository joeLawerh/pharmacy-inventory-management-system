/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.controller.managedBean;

import com.pharmaderm.entities.Counter;
import com.pharmaderm.entities.DrugsReturned;
import com.pharmaderm.entities.Sales;
import com.pharmaderm.entities.SupplyReturn;
import com.pharmaderm.session.PharmadermSessionBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@ManagedBean
@SessionScoped
public class StockController implements Serializable {

    /**
     * Creates a new instance of StockController
     */
    @EJB
    PharmadermSessionBean pharmadermSessionBean;
    Counter counter;
    Sales sales;
    DrugsReturned drugsReturned;
    SupplyReturn supplyReturn;
    List<Counter> listOfCounters = new ArrayList<Counter>();
    List<Counter> counterList = new ArrayList<Counter>();
    DataModel<Counter> dataModelCounter = new ArrayDataModel<Counter>();
    SelectItem[] counterSelect;
    SelectItem[] salesSelect;
    private Integer quantity;
    private String drugName;
    private Double pric;
    private Double net_amount;
    private Date expire;
    private Integer qty_dis;
    private Double total = 0.0;
    private Date salesDate, dis_date;
    private int total_sales_quantity = 0, dis_total = 0;
    private double total_sales_amount = 0.0, single_amount = 0.0, general_amount = 0.0, total_cost_amount = 0.0, single_cost_amount = 0.0, general_cost_amount = 0.0, single_profit, general_profit, total_sales_profit = 0.0;
    private int daily_quantity = 0, single_quantity = 0, general_quantity = 0;
    private double daily_amount = 0.0;
    private boolean print_report = false;
    private Date stDate;
    private Date endDate;
    private int tabIndex = 1;
    private boolean recp = true;
    private boolean printre = false;
    private CounterConvertor counterConvertor = new CounterConvertor();
    private Double cost_price, cost_amount;
    private Double daily_cost_amount = 0.0, daily_profit = 0.0;
    private String message = "";
    private Double amount_paid, balance;

    public void saveStock() {
        try {
            String UUIString = UUID.randomUUID().toString().substring(0, 5);
            counter.setCounterId(UUIString);
            counter.setDeleted("NO");
            counter.setDistributionDate(new Date());
            pharmadermSessionBean.CounterCreate(counter);
            setCounter(new Counter());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearStock() {
        setCounter(new Counter());
    }

    public List<Counter> getAllCounterDrugs() {
        try {
            return pharmadermSessionBean.counterGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Counter> getCounterDrugsAll() {
        try {
            return pharmadermSessionBean.counterGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Not available!1");
            return null;
        }
    }

    public void firstTab() {
        tabIndex = 0;
    }

    public void secondTab() {
        tabIndex = 2;
    }

    public List<Sales> getSalesInfo() {
        try {
            return pharmadermSessionBean.salesGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String maintainPage() {
        print_report = true;
        return "day.xhtml";
    }

    public void loadDrugsForSelection() {
        counterConvertor.allDrugs();
        counterList = counterConvertor.counteree;
    }

    public List<Counter> completeDrugName(String query) {
        List<Counter> autoComp = new ArrayList<Counter>();
        for (Iterator<Counter> it = counterList.iterator(); it.hasNext();) {
            Counter s = it.next();
            if (s.getDrugName().toLowerCase().startsWith(query.toLowerCase()) && !autoComp.contains(s)) {

                autoComp.add(s);

            } else {
            }
        }

        return autoComp;

    }

    public String displayInfoPage() {
        firstTab();
        print_report = true;
        return "report.xhtml";
    }

    public String general() {
        firstTab();
        print_report = true;
        return "general.xhtml";
    }

    public List<Sales> getAllSalesByDay() {
        try {
            return pharmadermSessionBean.findByDayReportType(salesDate);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void onRowEditDrugsInfo(RowEditEvent event) {
        try {
            counter = (Counter) event.getObject();
            boolean update = pharmadermSessionBean.counterUpdate(counter);
            if (update) {
                FacesMessage mge = new FacesMessage("Drug Updated for  ", ((Counter) event.getObject()).getDrugName());
                FacesContext.getCurrentInstance().addMessage(null, mge);
                System.out.println("Updated successfully!!");
            } else {
                FacesMessage mge = new FacesMessage("Drug Cannot be Updated for  ", ((Counter) event.getObject()).getDrugName());
                FacesContext.getCurrentInstance().addMessage(null, mge);
                System.out.println("Unable to update drug Info");
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Unable to update!!");
        }
    }

    public List<Sales> getSingleDrugReport() {
        try {
            return pharmadermSessionBean.findBySingleDrug(drugName, stDate, endDate);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    class CounterConvertor implements Converter {

        public List<Counter> counteree = new ArrayList<Counter>();

        public void allDrugs() {
            counteree = pharmadermSessionBean.counterGetAll();
        }

        @Override
        public Object getAsObject(FacesContext context, UIComponent component, String submittedValue) {

            if (submittedValue.trim().equals("")) {
                return null;
            } else {
                try {
                    //int number = Integer.parseInt(submittedValue);  

                    for (Counter s : counteree) {
                        if (s.getCounterId().equals(submittedValue)) {
                            return s;
                        }
                    }

                } catch (NumberFormatException exception) {
                    throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, "Conversion Error", "Not a valid Drug!!"));
                }
            }

            return null;
        }

        @Override
        public String getAsString(FacesContext context, UIComponent component, Object value) {
            if (value == null || value.equals("")) {
                return "";
            } else {
                return String.valueOf(((Counter) value).getCounterId());
            }

        }
    }

    public List<Sales> getGeneralRportOnSales() {
        try {
            return pharmadermSessionBean.generalSalesQuery(stDate, endDate);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void addList() {
        try {
            String Ui = UUID.randomUUID().toString().substring(0, 4);
            counter.setCounterId(Ui);
            counter.setDrugQuantity(quantity);
            counter.setPrice(net_amount);
            counter.setCostPrice(cost_amount);
            if (net_amount <= 0) {
                message = " Please Quantity is greater than that in stock for drug " + counter.getDrugName();
            } else {
                message = "";
                listOfCounters.add(counter);
                total += net_amount;
                Date ddd = counter.getExpiryDate();
                Long dt = ddd.getTime();
                Date to = new Date();
                Long de = to.getTime();
                int days = (int) ((dt - de) / 1000);
                int realDays = (days / 86400);
                FacesMessage facesMessage = new FacesMessage(realDays + "  Days More for  " + counter.getDrugName() + "  to Expire.!! ");
                facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
                setCounter(new Counter());
                setDrugName("");
                setQuantity(0);
                setQty_dis(0);
                setNet_amount(0.0);
                setPric(0.0);
                setCost_amount(0.0);
            }
        } catch (Exception e) {
            System.out.println("Not available");
            e.printStackTrace();
        }
    }

    public List<Counter> getCounterDrugsByDates() {
        try {
            return pharmadermSessionBean.findByDates(dis_date);
        } catch (Exception e) {
            System.out.println("Unable to find!!");
            e.printStackTrace();
        }
        return null;
    }

    public void calculateChange() {
        try {
            balance = amount_paid - total;
        } catch (Exception e) {
            System.out.println("Error Occured in getting balance: " + e.getLocalizedMessage());
        }
    }

    public void calculateNetPrice() {
        try {
            if (quantity <= counter.getDrugQuantity()) {
                net_amount = quantity * counter.getPrice();
                cost_amount = quantity * counter.getCostPrice();
                setNet_amount(net_amount);
                setCost_amount(cost_amount);
            } else {
                net_amount = 0.0;
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Can't calculate price!!");
        }
    }

    public String findDatePage() {
        firstTab();
        return "page.xhtml";
    }

    public void findDrugExpiryPrice() {
        try {
            //System.out.println("Drug Name is:" + counter.getDrugName());
            counter = pharmadermSessionBean.findByDrugName(counter.getDrugName());
            if (counter != null) {
                pric = counter.getPrice();
                setPric(pric);
                qty_dis = counter.getDrugQuantity();
                setQty_dis(qty_dis);
                expire = counter.getExpiryDate();
                setExpire(expire);
            } else {
                System.out.println("Drug Name Invalid!!");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeItem() {
        try {
            for (Counter coun : listOfCounters) {
                int dd = dataModelCounter.getRowIndex() + 1;
                total -= coun.getPrice();
                if (total < 0.0) {
                    total = 0.0;
                }
                listOfCounters.remove(dd);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void claerList() {
        listOfCounters.clear();
        setCounter(new Counter());
        recp = true;
        printre = false;
        setTotal(0.0);
        setAmount_paid(0.0);
        setBalance(0.0);
    }

    public void enterSales() {
        try {
            for (Counter count : listOfCounters) {
                counter = pharmadermSessionBean.findByDrugName(count.getDrugName());
                if (counter != null) {
                    int qtyt = counter.getDrugQuantity();
                    int qtt = count.getDrugQuantity();
                    int rem = qtyt - qtt;
                    if (rem > 0) {
                        counter.getDrugQuantity();
                        pharmadermSessionBean.counterDelete(counter);
                        counter.setDrugQuantity(rem);
                        pharmadermSessionBean.counterUpdate(counter);
                    } else {
                        counter.getCounterId();
                        pharmadermSessionBean.counterDelete(counter);
                    }
                    sales.setDeleted("NO");
                    sales.setDrugName(count.getDrugName());
                    sales.setDrugPrice(count.getPrice());
                    sales.setCostAmount(count.getCostPrice());
                    sales.setSalesDate(new Date());
                    sales.setSalesId(count.getCounterId());
                    sales.setDrugQuantity(count.getDrugQuantity());
                    pharmadermSessionBean.salesCreate(sales);
                    recp = false;
                    printre = true;
                    setSales(new Sales());
                } else {
                    System.out.println("Unable to find Matching tag!!");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void patietntsDrugsSave() {
        try {
            String uui = UUID.randomUUID().toString().substring(0, 5);
            drugsReturned.setReturnedId(uui);
            drugsReturned.setDeleted("NO");
            drugsReturned.setRecievedBy("Counter Users");
            drugsReturned.setDateReturned(new Date());
            pharmadermSessionBean.patientsDrugsCreate(drugsReturned);
            counter = pharmadermSessionBean.findByDrugName(drugsReturned.getDrugName());
            if (counter != null) {
                int qq = counter.getDrugQuantity();
                int qqty = drugsReturned.getQuantity();
                int total1 = qq + qqty;
                counter.getDrugQuantity();
                pharmadermSessionBean.counterDelete(counter);
                counter.setDrugQuantity(total1);
                pharmadermSessionBean.counterUpdate(counter);
            } else {
                System.out.println("Drug Name Invalid!!");
            }
            setDrugsReturned(new DrugsReturned());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<DrugsReturned> getAllDrugsByCustomers() {
        try {
            return pharmadermSessionBean.allDrugsForCustomers();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void supplierDrugsSave() {
        try {
            String udd = UUID.randomUUID().toString().substring(0, 5);
            supplyReturn.setSupId(udd);
            supplyReturn.setDeleted("NO");
            supplyReturn.setReturnDate(new Date());
            pharmadermSessionBean.suppliersDrugsCreate(supplyReturn);
            setSupplyReturn(new SupplyReturn());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<SupplyReturn> getAllSuppliersDrugs() {
        try {
            return pharmadermSessionBean.supplierisDrugsGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public StockController() {
        sales = new Sales();
        counter = new Counter();
        drugsReturned = new DrugsReturned();
        supplyReturn = new SupplyReturn();
    }

    public PharmadermSessionBean getPharmadermSessionBean() {
        return pharmadermSessionBean;
    }

    public void setPharmadermSessionBean(PharmadermSessionBean pharmadermSessionBean) {
        this.pharmadermSessionBean = pharmadermSessionBean;
    }

    public Date getStDate() {
        return stDate;
    }

    public void setStDate(Date stDate) {
        this.stDate = stDate;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Counter getCounter() {
        return counter;
    }

    public void setCounter(Counter counter) {
        this.counter = counter;
    }

    public double getSingle_amount() {
        single_amount = 0.0;
        for (Sales ss : getSingleDrugReport()) {
            single_amount += ss.getDrugPrice();
        }
        return single_amount;
    }

    public void setSingle_amount(double single_amount) {
        this.single_amount = single_amount;
    }

    public double getGeneral_amount() {
        general_amount = 0.0;
        for (Sales sd : getGeneralRportOnSales()) {
            general_amount += sd.getDrugPrice();
        }
        return general_amount;
    }

    public void setGeneral_amount(double general_amount) {
        this.general_amount = general_amount;
    }

    public int getSingle_quantity() {
        for (Sales ss : getSingleDrugReport()) {
            single_quantity += ss.getDrugQuantity();
        }
        return single_quantity;
    }

    public void setSingle_quantity(int single_quantity) {
        this.single_quantity = single_quantity;
    }

    public int getGeneral_quantity() {
        for (Sales sd : getGeneralRportOnSales()) {
            general_quantity += sd.getDrugQuantity();
        }
        return general_quantity;
    }

    public void setGeneral_quantity(int general_quantity) {
        this.general_quantity = general_quantity;
    }

    public Sales getSales() {
        return sales;
    }

    public void setSales(Sales sales) {
        this.sales = sales;
    }

    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    public Date getDis_date() {
        return dis_date;
    }

    public void setDis_date(Date dis_date) {
        this.dis_date = dis_date;
    }

    public int getTabIndex() {
        return tabIndex;
    }

    public void setTabIndex(int tabIndex) {
        this.tabIndex = tabIndex;
    }

    public List<Counter> getListOfCounters() {
        return listOfCounters;
    }

    public void setListOfCounters(List<Counter> listOfCounters) {
        this.listOfCounters = listOfCounters;
    }

    public DataModel<Counter> getDataModelCounter() {
        return dataModelCounter;
    }

    public void setDataModelCounter(DataModel<Counter> dataModelCounter) {
        this.dataModelCounter = dataModelCounter;
    }

    public List<Counter> getCounterList() {
        counterConvertor.allDrugs();
        counterList = counterConvertor.counteree;
        return counterList;
    }

    public void setCounterList(List<Counter> counterList) {
        this.counterList = counterList;
    }

    public SelectItem[] getSalesSelect() {
        int count = 0;
        List<Sales> listOfSaleses1 = new ArrayList<Sales>();
        listOfSaleses1 = pharmadermSessionBean.salesGetAll();
        salesSelect = new SelectItem[(listOfSaleses1.size())];
        for (Sales sale : listOfSaleses1) {
            salesSelect[count] = new SelectItem(sale.getDrugName(), sale.getDrugName());
            count++;
        }
        return salesSelect;
    }

    public void setSalesSelect(SelectItem[] salesSelect) {
        this.salesSelect = salesSelect;
    }

    public SelectItem[] getCounterSelect() {
        int count = 0;
        List<Counter> listOfCounters1 = new ArrayList<Counter>();
        listOfCounters1 = pharmadermSessionBean.counterGetAll();
        counterSelect = new SelectItem[(listOfCounters1.size())];
        for (Counter co : listOfCounters1) {
            counterSelect[count] = new SelectItem(co.getDrugName(), co.getDrugName());
            count++;
        }
        return counterSelect;
    }

    public void setCounterSelect(SelectItem[] counterSelect) {
        this.counterSelect = counterSelect;
    }

    public DrugsReturned getDrugsReturned() {
        return drugsReturned;
    }

    public void setDrugsReturned(DrugsReturned drugsReturned) {
        this.drugsReturned = drugsReturned;
    }

    public SupplyReturn getSupplyReturn() {
        return supplyReturn;
    }

    public void setSupplyReturn(SupplyReturn supplyReturn) {
        this.supplyReturn = supplyReturn;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public Double getTotal() {
        return total;
    }

    public int getTotal_sales_quantity() {
        total_sales_quantity = 0;
        for (Sales sale : getSalesInfo()) {
            total_sales_quantity += sale.getDrugQuantity();
        }
        return total_sales_quantity;
    }

    public void setTotal_sales_quantity(int total_sales_quantity) {
        this.total_sales_quantity = total_sales_quantity;
    }

    public double getTotal_sales_amount() {
        total_sales_amount = 0.0;
        for (Sales sal : getSalesInfo()) {
            total_sales_amount += sal.getDrugPrice();
        }
        return total_sales_amount;
    }

    public void setTotal_sales_amount(double total_sales_amount) {
        this.total_sales_amount = total_sales_amount;
    }

    public CounterConvertor getCounterConvertor() {
        return counterConvertor;
    }

    public void setCounterConvertor(CounterConvertor counterConvertor) {
        this.counterConvertor = counterConvertor;
    }

    public int getDaily_quantity() {
        daily_quantity = 0;
        for (Sales sas : getAllSalesByDay()) {
            daily_quantity += sas.getDrugQuantity();
        }
        return daily_quantity;
    }

    public void setDaily_quantity(int daily_quantity) {
        this.daily_quantity = daily_quantity;
    }

    public double getDaily_amount() {
        daily_amount = 0.0;
        for (Sales sas : getAllSalesByDay()) {
            daily_amount += sas.getDrugPrice();
        }
        return daily_amount;
    }

    public int getDis_total() {
        for (Counter cc : getCounterDrugsByDates()) {
            dis_total += cc.getDrugQuantity();
        }
        return dis_total;
    }

    public void setDis_total(int dis_total) {
        this.dis_total = dis_total;
    }

    public void setDaily_amount(double daily_amount) {
        this.daily_amount = daily_amount;
    }

    public boolean isPrint_report() {
        return print_report;
    }

    public void setPrint_report(boolean print_report) {
        this.print_report = print_report;
    }

    public void setTotal(Double total) {
        this.total = total;
    }

    public Double getPric() {
        return pric;
    }

    public void setPric(Double pric) {
        this.pric = pric;
    }

    public Double getNet_amount() {
        return net_amount;
    }

    public Integer getQty_dis() {
        return qty_dis;
    }

    public void setQty_dis(Integer qty_dis) {
        this.qty_dis = qty_dis;
    }

    public void setNet_amount(Double net_amount) {
        this.net_amount = net_amount;
    }

    public Date getExpire() {
        return expire;
    }

    public void setExpire(Date expire) {
        this.expire = expire;
    }

    public boolean isRecp() {
        return recp;
    }

    public void setRecp(boolean recp) {
        this.recp = recp;
    }

    public boolean isPrintre() {
        return printre;
    }

    public void setPrintre(boolean printre) {
        this.printre = printre;
    }

    public Double getCost_price() {
        return cost_price;
    }

    public void setCost_price(Double cost_price) {
        this.cost_price = cost_price;
    }

    public Double getCost_amount() {
        return cost_amount;
    }

    public void setCost_amount(Double cost_amount) {
        this.cost_amount = cost_amount;
    }

    public double getTotal_cost_amount() {
        total_cost_amount = 0.0;
        for (Sales sal : getSalesInfo()) {
            total_cost_amount += sal.getCostAmount();
        }
        return total_cost_amount;
    }

    public void setTotal_cost_amount(double total_cost_amount) {
        this.total_cost_amount = total_cost_amount;
    }

    public double getSingle_cost_amount() {
        single_cost_amount = 0.0;
        for (Sales sas : getAllSalesByDay()) {
            single_cost_amount += sas.getCostAmount();
        }
        return single_cost_amount;
    }

    public void setSingle_cost_amount(double single_cost_amount) {
        this.single_cost_amount = single_cost_amount;
    }

    public double getGeneral_cost_amount() {
        general_cost_amount = 0.0;
        for (Sales sd : getGeneralRportOnSales()) {
            general_cost_amount += sd.getCostAmount();
        }
        return general_cost_amount;
    }

    public void setGeneral_cost_amount(double general_cost_amount) {
        this.general_cost_amount = general_cost_amount;
    }

    public double getSingle_profit() {
        single_profit = 0.0;
        single_cost_amount = 0.0;
        for (Sales sas : getAllSalesByDay()) {
            single_cost_amount += sas.getCostAmount();
        }
        daily_amount = 0.0;
        for (Sales sas : getAllSalesByDay()) {
            daily_amount += sas.getDrugPrice();
        }
        single_profit = daily_amount - single_cost_amount;
        return single_profit;
    }

    public void setSingle_profit(double single_profit) {

        this.single_profit = single_profit;
    }

    public double getGeneral_profit() {
        general_amount = 0.0;
        for (Sales sd : getGeneralRportOnSales()) {
            general_amount += sd.getDrugPrice();
        }
        general_cost_amount = 0.0;
        for (Sales sd : getGeneralRportOnSales()) {
            general_cost_amount += sd.getCostAmount();
        }
        general_profit = general_amount - general_cost_amount;
        return general_profit;
    }

    public void setGeneral_profit(double general_profit) {
        this.general_profit = general_profit;
    }

    public Double getDaily_cost_amount() {
        daily_cost_amount = 0.0;
        for (Sales ss : getSingleDrugReport()) {
            daily_cost_amount += ss.getCostAmount();
        }
        return daily_cost_amount;
    }

    public void setDaily_cost_amount(Double daily_cost_amount) {
        this.daily_cost_amount = daily_cost_amount;
    }

    public Double getDaily_profit() {
        daily_profit = 0.0;
        single_amount = 0.0;
        for (Sales ss : getSingleDrugReport()) {
            single_amount += ss.getDrugPrice();
        }
        daily_cost_amount = 0.0;
        for (Sales ss : getSingleDrugReport()) {
            daily_cost_amount += ss.getCostAmount();
        }
        daily_profit = daily_amount - daily_cost_amount;
        return daily_profit;
    }

    public void setDaily_profit(Double daily_profit) {
        this.daily_profit = daily_profit;
    }

    public double getTotal_sales_profit() {
        total_sales_amount = 0.0;
        for (Sales sal : getSalesInfo()) {
            total_sales_amount += sal.getDrugPrice();
        }

        total_cost_amount = 0.0;
        for (Sales sal : getSalesInfo()) {
            total_cost_amount += sal.getCostAmount();
        }

        total_sales_profit = total_sales_amount - total_cost_amount;

        return total_sales_profit;
    }

    public void setTotal_sales_profit(double total_sales_profit) {
        this.total_sales_profit = total_sales_profit;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Double getAmount_paid() {
        return amount_paid;
    }

    public void setAmount_paid(Double amount_paid) {
        this.amount_paid = amount_paid;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }

}
