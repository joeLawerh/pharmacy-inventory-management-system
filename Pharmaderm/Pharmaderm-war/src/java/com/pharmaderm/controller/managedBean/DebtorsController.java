/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.controller.managedBean;

import com.pharmaderm.entities.Debtors;
import com.pharmaderm.session.PharmadermSessionBean;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import org.primefaces.event.RowEditEvent;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@ManagedBean
@SessionScoped
public class DebtorsController implements Serializable {

    @EJB
    PharmadermSessionBean pharmadermSessionBean;
    Debtors debtors;
    DataModel<Debtors> debtorsDataModel = new ArrayDataModel<Debtors>();
    private double total;
    private double unit_p;
    private double balance_rem;
    private int qty;
    private double part_pay;
    private int t_qty = 0;
    private double t_amt = 0.0 , bal_rem = 0.0;
    private double t_part_pay = 0.0;

    public void saveDebtorsData() {
        try {
            String d_id = UUID.randomUUID().toString().substring(0, 6);
            total = qty * unit_p;
            balance_rem = total - part_pay;
            debtors.setCompId(d_id);
            debtors.setBalRem(balance_rem);
            debtors.setDateBought(new Date());
            debtors.setPartPay(part_pay);
            debtors.setTotalPrice(total);
            debtors.setQtySupplied(qty);
            debtors.setUnitPrice(unit_p);
            debtors.setStatus("Part-payed");
            pharmadermSessionBean.createDebtor(debtors);
            setDebtors(new Debtors());
            setPart_pay(0.0);
            setUnit_p(0.0);
            setQty(0);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("not created!!");
        }
    }

    public List<Debtors> getFindDebtorsGetAll() {
        try {
            return pharmadermSessionBean.debtorsListGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void updateDetors() {
        try {
            debtors.setStatus("Completed");
            debtors.setPartPay(0.0);
            debtors.setBalRem(0.0);
            debtors.setDateBought(new Date());
            pharmadermSessionBean.debtorUpdate(debtors);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Successfully Updated");
        }
    }

    public void reset() {
        setDebtors(new Debtors());
        setPart_pay(0.0);
        setUnit_p(0.0);
        setQty(0);
        setTotal(0.0);
        setBalance_rem(0.0);
    }

    public void onRowEditDrugsInfoData(RowEditEvent event) {
        try {
            debtors = (Debtors) event.getObject();
            boolean update = pharmadermSessionBean.debtorUpdate(debtors);
            if (update) {
                FacesMessage mge = new FacesMessage("Item Updated for  ", ((Debtors) event.getObject()).getItemName());
                FacesContext.getCurrentInstance().addMessage(null, mge);
                System.out.println("Updated successfully!!");
            } else {
                FacesMessage mge = new FacesMessage("Item Cannot be Updated for  ", ((Debtors) event.getObject()).getItemName());
                FacesContext.getCurrentInstance().addMessage(null, mge);
                System.out.println("Unable to update item Info");
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Unable to update!!  " + e.getMessage());
        }
    }

    public void processDataItems() {
        debtors = debtorsDataModel.getRowData();
    }

    public List<Debtors> getDebtorsALl() {
        try {
            return pharmadermSessionBean.debtorsListGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public DebtorsController() {
        debtors = new Debtors();
    }

    public DataModel<Debtors> getDebtorsDataModel() {
        return debtorsDataModel;
    }

    public void setDebtorsDataModel(DataModel<Debtors> debtorsDataModel) {
        this.debtorsDataModel = debtorsDataModel;
    }

    public int getT_qty() {
        t_qty = 0;
        for (Debtors deb1 : getDebtorsALl()) {
            t_qty += deb1.getQtySupplied();
        }
        return t_qty;
    }

    public void setT_qty(int t_qty) {
        this.t_qty = t_qty;
    }

    public double getT_amt() {
        t_amt = 0.0;
        for (Debtors debtors1 : getDebtorsALl()) {
            t_amt += debtors1.getTotalPrice();
        }
        return t_amt;
    }

    public double getBal_rem() {
        bal_rem = 0.0;
        for (Debtors debt : getDebtorsALl()) {
            bal_rem += debt.getBalRem();
        }
        return bal_rem;
    }

    public void setBal_rem(double bal_rem) {
        this.bal_rem = bal_rem;
    }

    public void setT_amt(double t_amt) {
        this.t_amt = t_amt;
    }

    public double getT_part_pay() {
        t_part_pay = 0.0;
        for (Debtors deb : getDebtorsALl()) {
            t_part_pay += deb.getPartPay();
        }
        return t_part_pay;
    }

    public void setT_part_pay(double t_part_pay) {
        this.t_part_pay = t_part_pay;
    }

    public PharmadermSessionBean getPharmadermSessionBean() {
        return pharmadermSessionBean;
    }

    public void setPharmadermSessionBean(PharmadermSessionBean pharmadermSessionBean) {
        this.pharmadermSessionBean = pharmadermSessionBean;
    }

    public Debtors getDebtors() {
        return debtors;
    }

    public void setDebtors(Debtors debtors) {
        this.debtors = debtors;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getUnit_p() {
        return unit_p;
    }

    public void setUnit_p(double unit_p) {
        this.unit_p = unit_p;
    }

    public double getBalance_rem() {
        return balance_rem;
    }

    public void setBalance_rem(double balance_rem) {
        this.balance_rem = balance_rem;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public double getPart_pay() {
        return part_pay;
    }

    public void setPart_pay(double part_pay) {
        this.part_pay = part_pay;
    }
}
