/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.controller.managedBean;

import com.pharmaderm.entities.UserAccount;
import com.pharmaderm.entities.UserTrails;
import com.pharmaderm.session.PharmadermSessionBean;
import java.io.IOException;
import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import javax.ws.rs.core.CacheControl;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@ManagedBean
@SessionScoped
public class LoginManagedBeanController implements Serializable {

    /**
     * Creates a new instance of LoginManagedBeanController
     */
    @EJB
    PharmadermSessionBean pharmadermSessionBean;
    UserAccount userAccount;
    UserTrails userTrails;
    private String username;
    private String password;
    private String del;
    private String access;
    private String user;
    private String oldPass;
    private String newPass;
    private String confirmPass;
    private Date date;
    private String dte;
    private String time;

    public String validateUserCredentials() {
        try {
            userAccount = pharmadermSessionBean.checkDetails(username, password);
            if (userAccount != null) {
                del = userAccount.getDeleted();
                access = userAccount.getAccessLevel();
                if (access.equalsIgnoreCase("administrator") && del.equalsIgnoreCase("NO")) {
                    String Uu = UUID.randomUUID().toString().substring(0, 5);
                    userTrails.setActionDateTime(new Date());
                    userTrails.setActionId(Uu);
                    userTrails.setUsername(username);
                    userTrails.setActions(username + "" + "was able to login into the administrator interafec for his administrative works!!");
                    pharmadermSessionBean.userTrailsCreate(userTrails);
                    setUserTrails(new UserTrails());
                    return "admin.xhtml";
                } else if (access.equalsIgnoreCase("user") && del.equalsIgnoreCase("NO")) {
                    String Uu = UUID.randomUUID().toString().substring(0, 5);
                    userTrails.setActionDateTime(new Date());
                    userTrails.setActionId(Uu);
                    userTrails.setUsername(username);
                    userTrails.setActions(username + "" + "was able to login, and was attending to customers, i.e sales point!");
                    pharmadermSessionBean.userTrailsCreate(userTrails);
                    setUserTrails(new UserTrails());
                    return "user.xhtml";
                } else if (access.equalsIgnoreCase("counter") && del.equalsIgnoreCase("NO")) {
                    String Uu = UUID.randomUUID().toString().substring(0, 5);
                    userTrails.setActionDateTime(new Date());
                    userTrails.setActionId(Uu);
                    userTrails.setUsername(username);
                    userTrails.setActions(username + "" + "was able to login, was taking stock!!");
                    pharmadermSessionBean.userTrailsCreate(userTrails);
                    setUserTrails(new UserTrails());
                    return "counter.xhtml";
                } else if (access.equalsIgnoreCase("counter and user") && del.equalsIgnoreCase("NO")) {
                    String Uu = UUID.randomUUID().toString().substring(0, 5);
                    userTrails.setActionDateTime(new Date());
                    userTrails.setActionId(Uu);
                    userTrails.setUsername(username);
                    userTrails.setActions(username + "" + "was able to login, doing sales mainly and taking in stock of drugs!!");
                    pharmadermSessionBean.userTrailsCreate(userTrails);
                    setUserTrails(new UserTrails());
                    return "both.xhtml";
                } else {
                    return "index.xhtml?faces-redirect=true";
                }
            } else {
                return "index.xhtml?faces-redirect=true";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "index.xhtml?faces-redirect=true";
        }
    }

    public void logoutPage() throws IOException {
        getLoggedInUsername();
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpSession httpSession = (HttpSession) facesContext.getExternalContext().getSession(false);
        httpSession.invalidate();
        setPharmadermSessionBean(new PharmadermSessionBean());
        FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
        CacheControl cacheControl = new CacheControl();
        cacheControl.isNoCache();
        cacheControl.isNoStore();
        cacheControl.isMustRevalidate();
        cacheControl.setNoCache(true);
        cacheControl.setMustRevalidate(false);
        cacheControl.setNoStore(true);
        cacheControl.setProxyRevalidate(false);
        FacesContext.getCurrentInstance().getExternalContext().redirect("http://localhost:8080/Pharmaderm-war/");
    }

    public String getLoggedInUsername() {
        return FacesContext.getCurrentInstance().getExternalContext().getRemoteUser();
    }

    public boolean isSignedIn() {
        return (getLoggedInUsername() != null);
    }

    public List<UserTrails> getAllUserTrails() {
        try {
            return pharmadermSessionBean.userTrailsGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void handleDateAndTime() {
        date = new Date();
        dte = date.getDate() + " / " + (date.getMonth() + 1) + " / " + (date.getYear() + 1900);
        time = date.getHours() + " : " + date.getMinutes() + " : " + date.getSeconds();
    }

    public void deactivateUser() {
        try {
            userAccount = pharmadermSessionBean.findByUsernameInUserAccount(user);
            if (userAccount != null) {
                userAccount.getDeleted();
                pharmadermSessionBean.userAccountDelete(userAccount);
                userAccount.setDeleted("YES");
                pharmadermSessionBean.userAccountUpdate(userAccount);
                FacesMessage facesMessage = new FacesMessage("User deactivated Successfully");
                facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            } else {
                System.out.println("Unable to reach target!!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Target Unreachable!!");
        }
    }

    public void activateUser() {
        try {
            userAccount = pharmadermSessionBean.findByUsernameInUserAccount(user);
            if (userAccount != null) {
                userAccount.getDeleted();
                pharmadermSessionBean.userAccountDelete(userAccount);
                userAccount.setDeleted("NO");
                pharmadermSessionBean.userAccountUpdate(userAccount);
                FacesMessage facesMessage = new FacesMessage("User deactivated Successfully");
                facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
                FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            } else {
                System.out.println("Unable to reach target!!");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Target Unreachable!!");
        }
    }

    public void changingPass() {
        try {
            userAccount = pharmadermSessionBean.findByPassword(oldPass);
            if (userAccount != null) {
                userAccount.getPassword();
                pharmadermSessionBean.userAccountDelete(userAccount);
                userAccount.setPassword(newPass);
                pharmadermSessionBean.userAccountUpdate(userAccount);
            } else {
                System.out.println("Target Unreachable");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkEnteries() {
        if (newPass.equals(confirmPass)) {
            changingPass();
            FacesMessage facesMessage = new FacesMessage("New Password Successfully Updated");
            facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            setUser("");
            setConfirmPass("");
            setNewPass("");
        } else {
            FacesMessage facesMessage = new FacesMessage("Passwords do not match, please try again!!");
            facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
        }
    }

    public void registerNewSystemUser() {
        try {
            String uuString = UUID.randomUUID().toString().substring(0, 5);
            userAccount.setUserId(uuString);
            userAccount.setDeleted("NO");
            userAccount.setRegDate(new Date());
            pharmadermSessionBean.userAccountCreate(userAccount);
            FacesMessage facesMessage = new FacesMessage("User Successfully Registered!");
            facesMessage.setSeverity(FacesMessage.SEVERITY_INFO);
            FacesContext.getCurrentInstance().addMessage(null, facesMessage);
            setUserAccount(new UserAccount());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<UserAccount> getAllSystemUsers() {
        try {
            return pharmadermSessionBean.userAccountGetAll();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public LoginManagedBeanController() {
        userAccount = new UserAccount();
        userTrails = new UserTrails();
    }

    public PharmadermSessionBean getPharmadermSessionBean() {
        return pharmadermSessionBean;
    }

    public void setPharmadermSessionBean(PharmadermSessionBean pharmadermSessionBean) {
        this.pharmadermSessionBean = pharmadermSessionBean;
    }

    public UserAccount getUserAccount() {
        return userAccount;
    }

    public void setUserAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

    public UserTrails getUserTrails() {
        return userTrails;
    }

    public void setUserTrails(UserTrails userTrails) {
        this.userTrails = userTrails;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDel() {
        return del;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getOldPass() {
        return oldPass;
    }

    public void setOldPass(String oldPass) {
        this.oldPass = oldPass;
    }

    public String getNewPass() {
        return newPass;
    }

    public void setNewPass(String newPass) {
        this.newPass = newPass;
    }

    public String getConfirmPass() {
        return confirmPass;
    }

    public void setConfirmPass(String confirmPass) {
        this.confirmPass = confirmPass;
    }

    public void setDel(String del) {
        this.del = del;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDte() {
        return dte;
    }

    public void setDte(String dte) {
        this.dte = dte;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }
}
