/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.controller.managedBean;

import com.pharmaderm.entities.Counter;
import com.pharmaderm.entities.CreditDrugs;
import com.pharmaderm.entities.PaymentDetails;
import com.pharmaderm.session.PharmadermSessionBean;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.model.ArrayDataModel;
import javax.faces.model.DataModel;
import javax.faces.model.SelectItem;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@ManagedBean
@SessionScoped
public class CreditAndPaymentController implements Serializable {

    /**
     * Creates a new instance of CreditAndPaymentController
     */
    @EJB
    PharmadermSessionBean pharmadermSessionBean;
    CreditDrugs creditDrugs;
    PaymentDetails paymentDetails;
    List<CreditDrugs> listOfCreditDrugses = new ArrayList<CreditDrugs>();
    DataModel<CreditDrugs> creditDataModel = new ArrayDataModel<CreditDrugs>();
    Counter counter;
    SelectItem[] creditSelectItems;
    SelectItem[] counterSelectItems;
    private String status = "Not Paid";
    private String compnayName;
    private double check_value;
    private double drug_amount;
    private double bal;
    private double cost;
    private int qty, total_qty = 0, t_qty;
    private double total_cost = 0.0, s_price;
    private String drugName;
    private boolean showPrint = false, showSave = true;
    private int activeIndex = 0;

    public void addToList() {
        try {
            String uuString = UUID.randomUUID().toString().substring(0, 5);
            creditDrugs.setDetId(uuString);
            creditDrugs.setDateBought(new Date());
            creditDrugs.setStatus(status);
            creditDrugs.setCost(cost);
            creditDrugs.setDrugName(drugName);
            creditDrugs.setCompanyName(compnayName);
            creditDrugs.setQuantity(qty);
            listOfCreditDrugses.add(creditDrugs);
            total_cost += cost;
            total_qty += qty;
            setCreditDrugs(new CreditDrugs());
            setCost(0.0);
            setS_price(0.0);
            setQty(0);
            setDrugName("");
            setT_qty(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void operationRemove() {
        try {
            for (CreditDrugs cred : listOfCreditDrugses) {
                int ind = creditDataModel.getRowIndex() + 1;
                total_cost -= cred.getCost();
                total_qty -= cred.getQuantity();
                if (total_cost < 0.0) {
                    total_cost = 0.0;
                }
                if (total_qty < 0) {
                    total_qty = 0;
                }
                listOfCreditDrugses.remove(ind);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Unable to remove item!!");
        }
    }

    public void clear() {
        listOfCreditDrugses.clear();
        total_cost = 0.0;
        total_qty = 0;
        showPrint = false;
        showSave = true;
        setCost(0.0);
        setTotal_cost(0.0);
        setTotal_qty(0);
    }

    public void checkDrugQuantity() {
        try {
            counter = pharmadermSessionBean.findByDrugName(drugName);
            if (counter != null) {
                t_qty = counter.getDrugQuantity();
                setT_qty(t_qty);
                s_price = counter.getPrice();
                setS_price(s_price);
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Can't find drug!!");
        }
    }

    //control tabs movement
    public void tabONeShow() {
        activeIndex = 0;
    }

    public void tabTwoShow() {
        activeIndex = 1;
    }

    public void tabThreeShow() {
        activeIndex = 2;
    }

    //save drugs 
    public void processDrugPrice() {
        try {
            if (qty < t_qty) {
                double tt = qty * s_price;
                setCost(tt);
            } else {
                setCost(0.0);
            }
        } catch (Exception e) {
            System.out.println("Unable to multiply");
            e.printStackTrace();
        }
    }

    public void processInformation() {
        try {
            for (CreditDrugs credit : listOfCreditDrugses) {
                counter = pharmadermSessionBean.findByDrugName(credit.getDrugName());
                if (counter != null) {
                    System.out.println("Drug Name is found!! wow@#");
                    int qqtt = counter.getDrugQuantity();
                    int tty = credit.getQuantity();
                    int total = qqtt - tty;
                    if (total > 0) {
                        counter.getDrugQuantity();
                        pharmadermSessionBean.counterDelete(counter);
                        counter.setDrugQuantity(total);
                        pharmadermSessionBean.counterUpdate(counter);
                    } else {
                        counter.getCounterId();
                        pharmadermSessionBean.counterDelete(counter);
                    }
                } else {
                    System.out.println("Many Drugs By the same name was found, don't know which to update!!");
                }
                pharmadermSessionBean.creditDrugsCreate(credit);
                setCompnayName("");

                showSave = false;
                showPrint = true;
            }
            // listOfCreditDrugses.clear();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Unable to save Drugs!!");
        }
    }

    public List<CreditDrugs> getAllClearedCompanies() {
        try {
            return pharmadermSessionBean.findByCompanies(compnayName, status);
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void clearCompanies() {
        try {
            for (CreditDrugs credit : getAllClearedCompanies()) {
                credit.getStatus();
                pharmadermSessionBean.creditDrugsDelete(credit);
                credit.setStatus("Paid");
                pharmadermSessionBean.creditDrugsUpdate(credit);
                setCompnayName("");
                setStatus("");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<CreditDrugs> getTotalCreditOwners() {
        try {
            return pharmadermSessionBean.allDrugsOnCredit();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public CreditAndPaymentController() {
        creditDrugs = new CreditDrugs();
        paymentDetails = new PaymentDetails();
        counter = new Counter();
    }

    public Counter getCounter() {
        return counter;
    }

    public void setCounter(Counter counter) {
        this.counter = counter;
    }

    public SelectItem[] getCounterSelectItems() {
        int count = 0;
        List<Counter> listOfCounters = new ArrayList<Counter>();
        listOfCounters = pharmadermSessionBean.counterGetAll();
        counterSelectItems = new SelectItem[(listOfCounters.size())];
        for (Counter count1 : listOfCounters) {
            counterSelectItems[count] = new SelectItem(count1.getDrugName(), count1.getDrugName());
            count++;
        }
        return counterSelectItems;
    }

    public void setCounterSelectItems(SelectItem[] counterSelectItems) {
        this.counterSelectItems = counterSelectItems;
    }

    public SelectItem[] getCreditSelectItems() {
        int count = 0;
        List<CreditDrugs> listOfCreditDrugses1 = new ArrayList<CreditDrugs>();
        listOfCreditDrugses1 = pharmadermSessionBean.allDrugsOnCredit();
        creditSelectItems = new SelectItem[(listOfCreditDrugses1.size())];
        for (CreditDrugs credit : listOfCreditDrugses1) {
            creditSelectItems[count] = new SelectItem(credit.getCompanyName(), credit.getCompanyName());
            count++;
        }
        return creditSelectItems;
    }

    public void setCreditSelectItems(SelectItem[] creditSelectItems) {
        this.creditSelectItems = creditSelectItems;
    }

    public PharmadermSessionBean getPharmadermSessionBean() {
        return pharmadermSessionBean;
    }

    public void setPharmadermSessionBean(PharmadermSessionBean pharmadermSessionBean) {
        this.pharmadermSessionBean = pharmadermSessionBean;
    }

    public CreditDrugs getCreditDrugs() {
        return creditDrugs;
    }

    public void setCreditDrugs(CreditDrugs creditDrugs) {
        this.creditDrugs = creditDrugs;
    }

    public PaymentDetails getPaymentDetails() {
        return paymentDetails;
    }

    public void setPaymentDetails(PaymentDetails paymentDetails) {
        this.paymentDetails = paymentDetails;
    }

    public List<CreditDrugs> getListOfCreditDrugses() {
        return listOfCreditDrugses;
    }

    public void setListOfCreditDrugses(List<CreditDrugs> listOfCreditDrugses) {
        this.listOfCreditDrugses = listOfCreditDrugses;
    }

    public int getQty() {
        return qty;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getTotal_qty() {
        return total_qty;
    }

    public void setTotal_qty(int total_qty) {
        this.total_qty = total_qty;
    }

    public DataModel<CreditDrugs> getCreditDataModel() {
        return creditDataModel;
    }

    public void setCreditDataModel(DataModel<CreditDrugs> creditDataModel) {
        this.creditDataModel = creditDataModel;
    }

    public String getStatus() {
        return status;
    }

    public int getT_qty() {
        return t_qty;
    }

    public void setT_qty(int t_qty) {
        this.t_qty = t_qty;
    }

    public double getS_price() {
        return s_price;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public void setS_price(double s_price) {
        this.s_price = s_price;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCompnayName() {
        return compnayName;
    }

    public void setCompnayName(String compnayName) {
        this.compnayName = compnayName;
    }

    public double getCheck_value() {
        return check_value;
    }

    public void setCheck_value(double check_value) {
        this.check_value = check_value;
    }

    public double getDrug_amount() {
        return drug_amount;
    }

    public void setDrug_amount(double drug_amount) {
        this.drug_amount = drug_amount;
    }

    public double getBal() {
        return bal;
    }

    public void setBal(double bal) {
        this.bal = bal;
    }

    public double getCost() {
        return cost;
    }

    public void setCost(double cost) {
        this.cost = cost;
    }

    public double getTotal_cost() {
        return total_cost;
    }

    public void setTotal_cost(double total_cost) {
        this.total_cost = total_cost;
    }

    public boolean isShowPrint() {
        return showPrint;
    }

    public void setShowPrint(boolean showPrint) {
        this.showPrint = showPrint;
    }

    public boolean isShowSave() {
        return showSave;
    }

    public void setShowSave(boolean showSave) {
        this.showSave = showSave;
    }

    public int getActiveIndex() {
        return activeIndex;
    }

    public void setActiveIndex(int activeIndex) {
        this.activeIndex = activeIndex;
    }

}
