package com.pharmaderm.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-06-22T07:08:11")
@StaticMetamodel(CreditDrugs.class)
public class CreditDrugs_ { 

    public static volatile SingularAttribute<CreditDrugs, String> patientName;
    public static volatile SingularAttribute<CreditDrugs, String> status;
    public static volatile SingularAttribute<CreditDrugs, String> drugName;
    public static volatile SingularAttribute<CreditDrugs, String> detId;
    public static volatile SingularAttribute<CreditDrugs, Integer> quantity;
    public static volatile SingularAttribute<CreditDrugs, Date> dateBought;
    public static volatile SingularAttribute<CreditDrugs, String> companyName;
    public static volatile SingularAttribute<CreditDrugs, Double> cost;

}