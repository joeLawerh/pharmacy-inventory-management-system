package com.pharmaderm.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-06-22T07:08:11")
@StaticMetamodel(SupplyReturn.class)
public class SupplyReturn_ { 

    public static volatile SingularAttribute<SupplyReturn, String> supId;
    public static volatile SingularAttribute<SupplyReturn, String> reason;
    public static volatile SingularAttribute<SupplyReturn, String> drugName;
    public static volatile SingularAttribute<SupplyReturn, Integer> qty;
    public static volatile SingularAttribute<SupplyReturn, Date> returnDate;
    public static volatile SingularAttribute<SupplyReturn, String> deleted;

}