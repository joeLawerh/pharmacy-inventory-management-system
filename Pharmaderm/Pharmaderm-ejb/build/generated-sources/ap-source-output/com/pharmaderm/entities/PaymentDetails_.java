package com.pharmaderm.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-06-22T07:08:11")
@StaticMetamodel(PaymentDetails.class)
public class PaymentDetails_ { 

    public static volatile SingularAttribute<PaymentDetails, String> payId;
    public static volatile SingularAttribute<PaymentDetails, String> checkNo;
    public static volatile SingularAttribute<PaymentDetails, Double> checkValue;
    public static volatile SingularAttribute<PaymentDetails, String> supplierName;
    public static volatile SingularAttribute<PaymentDetails, String> status;
    public static volatile SingularAttribute<PaymentDetails, String> drugName;
    public static volatile SingularAttribute<PaymentDetails, Double> supAmount;
    public static volatile SingularAttribute<PaymentDetails, Date> payDate;
    public static volatile SingularAttribute<PaymentDetails, Double> balanceLeft;
    public static volatile SingularAttribute<PaymentDetails, String> deleted;

}