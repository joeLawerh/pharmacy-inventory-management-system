package com.pharmaderm.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-06-22T07:08:11")
@StaticMetamodel(Debtors.class)
public class Debtors_ { 

    public static volatile SingularAttribute<Debtors, String> compId;
    public static volatile SingularAttribute<Debtors, String> itemName;
    public static volatile SingularAttribute<Debtors, Double> partPay;
    public static volatile SingularAttribute<Debtors, String> status;
    public static volatile SingularAttribute<Debtors, Integer> qtySupplied;
    public static volatile SingularAttribute<Debtors, Double> balRem;
    public static volatile SingularAttribute<Debtors, Double> unitPrice;
    public static volatile SingularAttribute<Debtors, Date> dateBought;
    public static volatile SingularAttribute<Debtors, String> companyName;
    public static volatile SingularAttribute<Debtors, Double> totalPrice;

}