package com.pharmaderm.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-06-22T07:08:11")
@StaticMetamodel(DrugsReturned.class)
public class DrugsReturned_ { 

    public static volatile SingularAttribute<DrugsReturned, Date> dateReturned;
    public static volatile SingularAttribute<DrugsReturned, String> reason;
    public static volatile SingularAttribute<DrugsReturned, String> drugName;
    public static volatile SingularAttribute<DrugsReturned, String> returnedId;
    public static volatile SingularAttribute<DrugsReturned, Integer> quantity;
    public static volatile SingularAttribute<DrugsReturned, String> recievedBy;
    public static volatile SingularAttribute<DrugsReturned, String> deleted;

}