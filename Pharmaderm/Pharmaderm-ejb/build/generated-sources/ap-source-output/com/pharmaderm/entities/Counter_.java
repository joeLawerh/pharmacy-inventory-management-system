package com.pharmaderm.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-06-22T07:08:11")
@StaticMetamodel(Counter.class)
public class Counter_ { 

    public static volatile SingularAttribute<Counter, String> counterId;
    public static volatile SingularAttribute<Counter, Integer> drugQuantity;
    public static volatile SingularAttribute<Counter, Double> costPrice;
    public static volatile SingularAttribute<Counter, Double> price;
    public static volatile SingularAttribute<Counter, Date> expiryDate;
    public static volatile SingularAttribute<Counter, String> drugName;
    public static volatile SingularAttribute<Counter, Date> distributionDate;
    public static volatile SingularAttribute<Counter, String> deleted;
    public static volatile SingularAttribute<Counter, String> counterName;

}