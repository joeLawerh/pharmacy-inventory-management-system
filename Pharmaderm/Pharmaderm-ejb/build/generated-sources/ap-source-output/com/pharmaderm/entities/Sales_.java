package com.pharmaderm.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-06-22T07:08:11")
@StaticMetamodel(Sales.class)
public class Sales_ { 

    public static volatile SingularAttribute<Sales, Double> drugPrice;
    public static volatile SingularAttribute<Sales, Double> costAmount;
    public static volatile SingularAttribute<Sales, Integer> drugQuantity;
    public static volatile SingularAttribute<Sales, Date> salesDate;
    public static volatile SingularAttribute<Sales, String> drugName;
    public static volatile SingularAttribute<Sales, String> salesId;
    public static volatile SingularAttribute<Sales, String> deleted;

}