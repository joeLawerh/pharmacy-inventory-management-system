package com.pharmaderm.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-06-22T07:08:11")
@StaticMetamodel(CounterReturned.class)
public class CounterReturned_ { 

    public static volatile SingularAttribute<CounterReturned, Date> dateReturned;
    public static volatile SingularAttribute<CounterReturned, String> reason;
    public static volatile SingularAttribute<CounterReturned, String> retId;
    public static volatile SingularAttribute<CounterReturned, String> drugName;
    public static volatile SingularAttribute<CounterReturned, String> deleted;
    public static volatile SingularAttribute<CounterReturned, Integer> qtyReturned;

}