package com.pharmaderm.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-06-22T07:08:11")
@StaticMetamodel(UserTrails.class)
public class UserTrails_ { 

    public static volatile SingularAttribute<UserTrails, String> username;
    public static volatile SingularAttribute<UserTrails, String> actionId;
    public static volatile SingularAttribute<UserTrails, Date> actionDateTime;
    public static volatile SingularAttribute<UserTrails, String> actions;

}