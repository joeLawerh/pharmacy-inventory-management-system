package com.pharmaderm.entities;

import java.util.Date;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2015-06-22T07:08:11")
@StaticMetamodel(SalesItems.class)
public class SalesItems_ { 

    public static volatile SingularAttribute<SalesItems, String> itemName;
    public static volatile SingularAttribute<SalesItems, Date> salesDate;
    public static volatile SingularAttribute<SalesItems, Double> totalSold;
    public static volatile SingularAttribute<SalesItems, String> status;
    public static volatile SingularAttribute<SalesItems, Double> amtPayed;
    public static volatile SingularAttribute<SalesItems, Double> balRem;
    public static volatile SingularAttribute<SalesItems, Double> unitPrice;
    public static volatile SingularAttribute<SalesItems, String> sId;
    public static volatile SingularAttribute<SalesItems, String> buyeeName;

}