/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@Entity
@Table(name = "counter")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Counter.findAll", query = "SELECT c FROM Counter c"),
    @NamedQuery(name = "Counter.findByCounterId", query = "SELECT c FROM Counter c WHERE c.counterId = :counterId"),
    @NamedQuery(name = "Counter.findByCounterName", query = "SELECT c FROM Counter c WHERE c.counterName = :counterName"),
    @NamedQuery(name = "Counter.findByDrugName", query = "SELECT c FROM Counter c WHERE c.drugName = :drugName"),
    @NamedQuery(name = "Counter.findByDrugQuantity", query = "SELECT c FROM Counter c WHERE c.drugQuantity = :drugQuantity"),
    @NamedQuery(name = "Counter.findByPrice", query = "SELECT c FROM Counter c WHERE c.price = :price"),
    @NamedQuery(name = "Counter.findByExpiryDate", query = "SELECT c FROM Counter c WHERE c.expiryDate = :expiryDate"),
    @NamedQuery(name = "Counter.findByDistributionDate", query = "SELECT c FROM Counter c WHERE c.distributionDate = :distributionDate"),
    @NamedQuery(name = "Counter.findByDeleted", query = "SELECT c FROM Counter c WHERE c.deleted = :deleted")})
public class Counter implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "counter_id")
    private String counterId;
    @Size(max = 200)
    @Column(name = "counter_name")
    private String counterName;
    @Size(max = 300)
    @Column(name = "drug_name")
    private String drugName;
    @Column(name = "drug_quantity")
    private Integer drugQuantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "price")
    private Double price;
    @Column(name = "cost_price")
    private Double costPrice;
    @Column(name = "expiry_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date expiryDate;
    @Column(name = "distribution_date")
    @Temporal(TemporalType.DATE)
    private Date distributionDate;
    @Size(max = 5)
    @Column(name = "deleted")
    private String deleted;

    public Counter() {
    }

    public Counter(String counterId) {
        this.counterId = counterId;
    }

    public String getCounterId() {
        return counterId;
    }

    public void setCounterId(String counterId) {
        this.counterId = counterId;
    }

    public String getCounterName() {
        return counterName;
    }

    public void setCounterName(String counterName) {
        this.counterName = counterName;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public Integer getDrugQuantity() {
        return drugQuantity;
    }

    public void setDrugQuantity(Integer drugQuantity) {
        this.drugQuantity = drugQuantity;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    public Date getDistributionDate() {
        return distributionDate;
    }

    public void setDistributionDate(Date distributionDate) {
        this.distributionDate = distributionDate;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    public Double getCostPrice() {
        return costPrice;
    }

    public void setCostPrice(Double costPrice) {
        this.costPrice = costPrice;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (counterId != null ? counterId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Counter)) {
            return false;
        }
        Counter other = (Counter) object;
        if ((this.counterId == null && other.counterId != null) || (this.counterId != null && !this.counterId.equals(other.counterId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pharmaderm.entities.Counter[ counterId=" + counterId + " ]";
    }

}
