/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@Entity
@Table(name = "counter_returned")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CounterReturned.findAll", query = "SELECT c FROM CounterReturned c"),
    @NamedQuery(name = "CounterReturned.findByRetId", query = "SELECT c FROM CounterReturned c WHERE c.retId = :retId"),
    @NamedQuery(name = "CounterReturned.findByQtyReturned", query = "SELECT c FROM CounterReturned c WHERE c.qtyReturned = :qtyReturned"),
    @NamedQuery(name = "CounterReturned.findByDrugName", query = "SELECT c FROM CounterReturned c WHERE c.drugName = :drugName"),
    @NamedQuery(name = "CounterReturned.findByReason", query = "SELECT c FROM CounterReturned c WHERE c.reason = :reason"),
    @NamedQuery(name = "CounterReturned.findByDateReturned", query = "SELECT c FROM CounterReturned c WHERE c.dateReturned = :dateReturned"),
    @NamedQuery(name = "CounterReturned.findByDeleted", query = "SELECT c FROM CounterReturned c WHERE c.deleted = :deleted")})
public class CounterReturned implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "ret_id")
    private String retId;
    @Column(name = "qty_returned")
    private Integer qtyReturned;
    @Size(max = 100)
    @Column(name = "drug_name")
    private String drugName;
    @Size(max = 200)
    @Column(name = "reason")
    private String reason;
    @Column(name = "date_returned")
    @Temporal(TemporalType.DATE)
    private Date dateReturned;
    @Size(max = 8)
    @Column(name = "deleted")
    private String deleted;

    public CounterReturned() {
    }

    public CounterReturned(String retId) {
        this.retId = retId;
    }

    public String getRetId() {
        return retId;
    }

    public void setRetId(String retId) {
        this.retId = retId;
    }

    public Integer getQtyReturned() {
        return qtyReturned;
    }

    public void setQtyReturned(Integer qtyReturned) {
        this.qtyReturned = qtyReturned;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getDateReturned() {
        return dateReturned;
    }

    public void setDateReturned(Date dateReturned) {
        this.dateReturned = dateReturned;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (retId != null ? retId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CounterReturned)) {
            return false;
        }
        CounterReturned other = (CounterReturned) object;
        if ((this.retId == null && other.retId != null) || (this.retId != null && !this.retId.equals(other.retId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pharmaderm.entities.CounterReturned[ retId=" + retId + " ]";
    }
    
}
