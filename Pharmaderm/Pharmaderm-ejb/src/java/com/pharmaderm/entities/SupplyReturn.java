/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@Entity
@Table(name = "supply_return")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SupplyReturn.findAll", query = "SELECT s FROM SupplyReturn s"),
    @NamedQuery(name = "SupplyReturn.findBySupId", query = "SELECT s FROM SupplyReturn s WHERE s.supId = :supId"),
    @NamedQuery(name = "SupplyReturn.findByDrugName", query = "SELECT s FROM SupplyReturn s WHERE s.drugName = :drugName"),
    @NamedQuery(name = "SupplyReturn.findByQty", query = "SELECT s FROM SupplyReturn s WHERE s.qty = :qty"),
    @NamedQuery(name = "SupplyReturn.findByReason", query = "SELECT s FROM SupplyReturn s WHERE s.reason = :reason"),
    @NamedQuery(name = "SupplyReturn.findByReturnDate", query = "SELECT s FROM SupplyReturn s WHERE s.returnDate = :returnDate"),
    @NamedQuery(name = "SupplyReturn.findByDeleted", query = "SELECT s FROM SupplyReturn s WHERE s.deleted = :deleted")})
public class SupplyReturn implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "sup_id")
    private String supId;
    @Size(max = 50)
    @Column(name = "drug_name")
    private String drugName;
    @Column(name = "qty")
    private Integer qty;
    @Size(max = 255)
    @Column(name = "reason")
    private String reason;
    @Column(name = "return_date")
    @Temporal(TemporalType.DATE)
    private Date returnDate;
    @Size(max = 8)
    @Column(name = "deleted")
    private String deleted;

    public SupplyReturn() {
    }

    public SupplyReturn(String supId) {
        this.supId = supId;
    }

    public String getSupId() {
        return supId;
    }

    public void setSupId(String supId) {
        this.supId = supId;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public Integer getQty() {
        return qty;
    }

    public void setQty(Integer qty) {
        this.qty = qty;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Date getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Date returnDate) {
        this.returnDate = returnDate;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (supId != null ? supId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SupplyReturn)) {
            return false;
        }
        SupplyReturn other = (SupplyReturn) object;
        if ((this.supId == null && other.supId != null) || (this.supId != null && !this.supId.equals(other.supId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pharmaderm.entities.SupplyReturn[ supId=" + supId + " ]";
    }
    
}
