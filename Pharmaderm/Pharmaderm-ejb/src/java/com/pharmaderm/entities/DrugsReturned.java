/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@Entity
@Table(name = "drugs_returned")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "DrugsReturned.findAll", query = "SELECT d FROM DrugsReturned d"),
    @NamedQuery(name = "DrugsReturned.findByReturnedId", query = "SELECT d FROM DrugsReturned d WHERE d.returnedId = :returnedId"),
    @NamedQuery(name = "DrugsReturned.findByDrugName", query = "SELECT d FROM DrugsReturned d WHERE d.drugName = :drugName"),
    @NamedQuery(name = "DrugsReturned.findByReason", query = "SELECT d FROM DrugsReturned d WHERE d.reason = :reason"),
    @NamedQuery(name = "DrugsReturned.findByDateReturned", query = "SELECT d FROM DrugsReturned d WHERE d.dateReturned = :dateReturned"),
    @NamedQuery(name = "DrugsReturned.findByRecievedBy", query = "SELECT d FROM DrugsReturned d WHERE d.recievedBy = :recievedBy"),
    @NamedQuery(name = "DrugsReturned.findByDeleted", query = "SELECT d FROM DrugsReturned d WHERE d.deleted = :deleted")})
public class DrugsReturned implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "returned_id")
    private String returnedId;
    @Size(max = 300)
    @Column(name = "drug_name")
    private String drugName;
    @Size(max = 300)
    @Column(name = "reason")
    private String reason;
    @Column(name = "quantity")
    private Integer quantity;
    @Column(name = "date_returned")
    @Temporal(TemporalType.TIMESTAMP)
    private Date dateReturned;
    @Size(max = 100)
    @Column(name = "recieved_by")
    private String recievedBy;
    @Size(max = 5)
    @Column(name = "deleted")
    private String deleted;

    public DrugsReturned() {
    }

    public DrugsReturned(String returnedId) {
        this.returnedId = returnedId;
    }

    public String getReturnedId() {
        return returnedId;
    }

    public void setReturnedId(String returnedId) {
        this.returnedId = returnedId;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Date getDateReturned() {
        return dateReturned;
    }

    public void setDateReturned(Date dateReturned) {
        this.dateReturned = dateReturned;
    }

    public String getRecievedBy() {
        return recievedBy;
    }

    public void setRecievedBy(String recievedBy) {
        this.recievedBy = recievedBy;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (returnedId != null ? returnedId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DrugsReturned)) {
            return false;
        }
        DrugsReturned other = (DrugsReturned) object;
        if ((this.returnedId == null && other.returnedId != null) || (this.returnedId != null && !this.returnedId.equals(other.returnedId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pharmaderm.entities.DrugsReturned[ returnedId=" + returnedId + " ]";
    }
    
}
