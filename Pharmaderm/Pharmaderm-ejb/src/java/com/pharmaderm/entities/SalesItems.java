/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@Entity
@Table(name = "sales_items")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "SalesItems.findAll", query = "SELECT s FROM SalesItems s"),
    @NamedQuery(name = "SalesItems.findBySId", query = "SELECT s FROM SalesItems s WHERE s.sId = :sId"),
    @NamedQuery(name = "SalesItems.findByBuyeeName", query = "SELECT s FROM SalesItems s WHERE s.buyeeName = :buyeeName"),
    @NamedQuery(name = "SalesItems.findByItemName", query = "SELECT s FROM SalesItems s WHERE s.itemName = :itemName"),
    @NamedQuery(name = "SalesItems.findByUnitPrice", query = "SELECT s FROM SalesItems s WHERE s.unitPrice = :unitPrice"),
    @NamedQuery(name = "SalesItems.findByTotalSold", query = "SELECT s FROM SalesItems s WHERE s.totalSold = :totalSold"),
    @NamedQuery(name = "SalesItems.findBySalesDate", query = "SELECT s FROM SalesItems s WHERE s.salesDate = :salesDate"),
    @NamedQuery(name = "SalesItems.findByBalRem", query = "SELECT s FROM SalesItems s WHERE s.balRem = :balRem"),
    @NamedQuery(name = "SalesItems.findByAmtPayed", query = "SELECT s FROM SalesItems s WHERE s.amtPayed = :amtPayed"),
    @NamedQuery(name = "SalesItems.findByStatus", query = "SELECT s FROM SalesItems s WHERE s.status = :status")})
public class SalesItems implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "s_id")
    private String sId;
    @Size(max = 100)
    @Column(name = "buyee_name")
    private String buyeeName;
    @Size(max = 150)
    @Column(name = "item_name")
    private String itemName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "unit_price")
    private Double unitPrice;
    @Column(name = "total_sold")
    private Double totalSold;
    @Column(name = "sales_date")
    @Temporal(TemporalType.DATE)
    private Date salesDate;
    @Column(name = "bal_rem")
    private Double balRem;
    @Column(name = "amt_payed")
    private Double amtPayed;
    @Size(max = 100)
    @Column(name = "status")
    private String status;

    public SalesItems() {
    }

    public SalesItems(String sId) {
        this.sId = sId;
    }

    public String getSId() {
        return sId;
    }

    public void setSId(String sId) {
        this.sId = sId;
    }

    public String getBuyeeName() {
        return buyeeName;
    }

    public void setBuyeeName(String buyeeName) {
        this.buyeeName = buyeeName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getTotalSold() {
        return totalSold;
    }

    public void setTotalSold(Double totalSold) {
        this.totalSold = totalSold;
    }

    public Date getSalesDate() {
        return salesDate;
    }

    public void setSalesDate(Date salesDate) {
        this.salesDate = salesDate;
    }

    public Double getBalRem() {
        return balRem;
    }

    public void setBalRem(Double balRem) {
        this.balRem = balRem;
    }

    public Double getAmtPayed() {
        return amtPayed;
    }

    public void setAmtPayed(Double amtPayed) {
        this.amtPayed = amtPayed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (sId != null ? sId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SalesItems)) {
            return false;
        }
        SalesItems other = (SalesItems) object;
        if ((this.sId == null && other.sId != null) || (this.sId != null && !this.sId.equals(other.sId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pharmaderm.entities.SalesItems[ sId=" + sId + " ]";
    }
    
}
