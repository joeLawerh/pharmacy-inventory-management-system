/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@Entity
@Table(name = "payment_details")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "PaymentDetails.findAll", query = "SELECT p FROM PaymentDetails p"),
    @NamedQuery(name = "PaymentDetails.findByPayId", query = "SELECT p FROM PaymentDetails p WHERE p.payId = :payId"),
    @NamedQuery(name = "PaymentDetails.findBySupplierName", query = "SELECT p FROM PaymentDetails p WHERE p.supplierName = :supplierName"),
    @NamedQuery(name = "PaymentDetails.findByDrugName", query = "SELECT p FROM PaymentDetails p WHERE p.drugName = :drugName"),
    @NamedQuery(name = "PaymentDetails.findBySupAmount", query = "SELECT p FROM PaymentDetails p WHERE p.supAmount = :supAmount"),
    @NamedQuery(name = "PaymentDetails.findByCheckNo", query = "SELECT p FROM PaymentDetails p WHERE p.checkNo = :checkNo"),
    @NamedQuery(name = "PaymentDetails.findByCheckValue", query = "SELECT p FROM PaymentDetails p WHERE p.checkValue = :checkValue"),
    @NamedQuery(name = "PaymentDetails.findByBalanceLeft", query = "SELECT p FROM PaymentDetails p WHERE p.balanceLeft = :balanceLeft"),
    @NamedQuery(name = "PaymentDetails.findByStatus", query = "SELECT p FROM PaymentDetails p WHERE p.status = :status"),
    @NamedQuery(name = "PaymentDetails.findByPayDate", query = "SELECT p FROM PaymentDetails p WHERE p.payDate = :payDate"),
    @NamedQuery(name = "PaymentDetails.findByDeleted", query = "SELECT p FROM PaymentDetails p WHERE p.deleted = :deleted")})
public class PaymentDetails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "pay_id")
    private String payId;
    @Size(max = 100)
    @Column(name = "supplier_name")
    private String supplierName;
    @Size(max = 50)
    @Column(name = "drug_name")
    private String drugName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "sup_amount")
    private Double supAmount;
    @Size(max = 150)
    @Column(name = "check_no")
    private String checkNo;
    @Column(name = "check_value")
    private Double checkValue;
    @Column(name = "balance_left")
    private Double balanceLeft;
    @Size(max = 20)
    @Column(name = "status")
    private String status;
    @Column(name = "pay_date")
    @Temporal(TemporalType.TIMESTAMP)
    private Date payDate;
    @Size(max = 8)
    @Column(name = "deleted")
    private String deleted;

    public PaymentDetails() {
    }

    public PaymentDetails(String payId) {
        this.payId = payId;
    }

    public String getPayId() {
        return payId;
    }

    public void setPayId(String payId) {
        this.payId = payId;
    }

    public String getSupplierName() {
        return supplierName;
    }

    public void setSupplierName(String supplierName) {
        this.supplierName = supplierName;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public Double getSupAmount() {
        return supAmount;
    }

    public void setSupAmount(Double supAmount) {
        this.supAmount = supAmount;
    }

    public String getCheckNo() {
        return checkNo;
    }

    public void setCheckNo(String checkNo) {
        this.checkNo = checkNo;
    }

    public Double getCheckValue() {
        return checkValue;
    }

    public void setCheckValue(Double checkValue) {
        this.checkValue = checkValue;
    }

    public Double getBalanceLeft() {
        return balanceLeft;
    }

    public void setBalanceLeft(Double balanceLeft) {
        this.balanceLeft = balanceLeft;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getPayDate() {
        return payDate;
    }

    public void setPayDate(Date payDate) {
        this.payDate = payDate;
    }

    public String getDeleted() {
        return deleted;
    }

    public void setDeleted(String deleted) {
        this.deleted = deleted;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (payId != null ? payId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof PaymentDetails)) {
            return false;
        }
        PaymentDetails other = (PaymentDetails) object;
        if ((this.payId == null && other.payId != null) || (this.payId != null && !this.payId.equals(other.payId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pharmaderm.entities.PaymentDetails[ payId=" + payId + " ]";
    }
    
}
