/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@Entity
@Table(name = "debtors")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Debtors.findAll", query = "SELECT d FROM Debtors d"),
    @NamedQuery(name = "Debtors.findByCompId", query = "SELECT d FROM Debtors d WHERE d.compId = :compId"),
    @NamedQuery(name = "Debtors.findByCompanyName", query = "SELECT d FROM Debtors d WHERE d.companyName = :companyName"),
    @NamedQuery(name = "Debtors.findByItemName", query = "SELECT d FROM Debtors d WHERE d.itemName = :itemName"),
    @NamedQuery(name = "Debtors.findByUnitPrice", query = "SELECT d FROM Debtors d WHERE d.unitPrice = :unitPrice"),
    @NamedQuery(name = "Debtors.findByTotalPrice", query = "SELECT d FROM Debtors d WHERE d.totalPrice = :totalPrice"),
    @NamedQuery(name = "Debtors.findByQtySupplied", query = "SELECT d FROM Debtors d WHERE d.qtySupplied = :qtySupplied"),
    @NamedQuery(name = "Debtors.findByPartPay", query = "SELECT d FROM Debtors d WHERE d.partPay = :partPay"),
    @NamedQuery(name = "Debtors.findByBalRem", query = "SELECT d FROM Debtors d WHERE d.balRem = :balRem"),
    @NamedQuery(name = "Debtors.findByStatus", query = "SELECT d FROM Debtors d WHERE d.status = :status"),
    @NamedQuery(name = "Debtors.findByDateBought", query = "SELECT d FROM Debtors d WHERE d.dateBought = :dateBought")})
public class Debtors implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "comp_id")
    private String compId;
    @Size(max = 100)
    @Column(name = "company_name")
    private String companyName;
    @Size(max = 100)
    @Column(name = "item_name")
    private String itemName;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "unit_price")
    private Double unitPrice;
    @Column(name = "total_price")
    private Double totalPrice;
    @Column(name = "qty_supplied")
    private Integer qtySupplied;
    @Column(name = "part_pay")
    private Double partPay;
    @Column(name = "bal_rem")
    private Double balRem;
    @Size(max = 50)
    @Column(name = "status")
    private String status;
    @Column(name = "date_bought")
    @Temporal(TemporalType.DATE)
    private Date dateBought;

    public Debtors() {
    }

    public Debtors(String compId) {
        this.compId = compId;
    }

    public String getCompId() {
        return compId;
    }

    public void setCompId(String compId) {
        this.compId = compId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public Double getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Integer getQtySupplied() {
        return qtySupplied;
    }

    public void setQtySupplied(Integer qtySupplied) {
        this.qtySupplied = qtySupplied;
    }

    public Double getPartPay() {
        return partPay;
    }

    public void setPartPay(Double partPay) {
        this.partPay = partPay;
    }

    public Double getBalRem() {
        return balRem;
    }

    public void setBalRem(Double balRem) {
        this.balRem = balRem;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Date getDateBought() {
        return dateBought;
    }

    public void setDateBought(Date dateBought) {
        this.dateBought = dateBought;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (compId != null ? compId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Debtors)) {
            return false;
        }
        Debtors other = (Debtors) object;
        if ((this.compId == null && other.compId != null) || (this.compId != null && !this.compId.equals(other.compId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pharmaderm.entities.Debtors[ compId=" + compId + " ]";
    }
    
}
