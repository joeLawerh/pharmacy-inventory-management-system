/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@Entity
@Table(name = "credit_drugs")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "CreditDrugs.findAll", query = "SELECT c FROM CreditDrugs c"),
    @NamedQuery(name = "CreditDrugs.findByDetId", query = "SELECT c FROM CreditDrugs c WHERE c.detId = :detId"),
    @NamedQuery(name = "CreditDrugs.findByCompanyName", query = "SELECT c FROM CreditDrugs c WHERE c.companyName = :companyName"),
    @NamedQuery(name = "CreditDrugs.findByPatientName", query = "SELECT c FROM CreditDrugs c WHERE c.patientName = :patientName"),
    @NamedQuery(name = "CreditDrugs.findByDrugName", query = "SELECT c FROM CreditDrugs c WHERE c.drugName = :drugName"),
    @NamedQuery(name = "CreditDrugs.findByQuantity", query = "SELECT c FROM CreditDrugs c WHERE c.quantity = :quantity"),
    @NamedQuery(name = "CreditDrugs.findByCost", query = "SELECT c FROM CreditDrugs c WHERE c.cost = :cost"),
    @NamedQuery(name = "CreditDrugs.findByDateBought", query = "SELECT c FROM CreditDrugs c WHERE c.dateBought = :dateBought"),
    @NamedQuery(name = "CreditDrugs.findByStatus", query = "SELECT c FROM CreditDrugs c WHERE c.status = :status")})
public class CreditDrugs implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "det_id")
    private String detId;
    @Size(max = 100)
    @Column(name = "company_name")
    private String companyName;
    @Size(max = 100)
    @Column(name = "patient_name")
    private String patientName;
    @Size(max = 100)
    @Column(name = "drug_name")
    private String drugName;
    @Column(name = "quantity")
    private Integer quantity;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "cost")
    private Double cost;
    @Column(name = "date_bought")
    @Temporal(TemporalType.DATE)
    private Date dateBought;
    @Size(max = 20)
    @Column(name = "status")
    private String status;

    public CreditDrugs() {
    }

    public CreditDrugs(String detId) {
        this.detId = detId;
    }

    public String getDetId() {
        return detId;
    }

    public void setDetId(String detId) {
        this.detId = detId;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getDrugName() {
        return drugName;
    }

    public void setDrugName(String drugName) {
        this.drugName = drugName;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Double getCost() {
        return cost;
    }

    public void setCost(Double cost) {
        this.cost = cost;
    }

    public Date getDateBought() {
        return dateBought;
    }

    public void setDateBought(Date dateBought) {
        this.dateBought = dateBought;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (detId != null ? detId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof CreditDrugs)) {
            return false;
        }
        CreditDrugs other = (CreditDrugs) object;
        if ((this.detId == null && other.detId != null) || (this.detId != null && !this.detId.equals(other.detId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pharmaderm.entities.CreditDrugs[ detId=" + detId + " ]";
    }
    
}
