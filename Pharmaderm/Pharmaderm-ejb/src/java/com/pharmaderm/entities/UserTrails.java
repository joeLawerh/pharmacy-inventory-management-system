/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.entities;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@Entity
@Table(name = "user_trails")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "UserTrails.findAll", query = "SELECT u FROM UserTrails u"),
    @NamedQuery(name = "UserTrails.findByActionId", query = "SELECT u FROM UserTrails u WHERE u.actionId = :actionId"),
    @NamedQuery(name = "UserTrails.findByUsername", query = "SELECT u FROM UserTrails u WHERE u.username = :username"),
    @NamedQuery(name = "UserTrails.findByActionDateTime", query = "SELECT u FROM UserTrails u WHERE u.actionDateTime = :actionDateTime")})
public class UserTrails implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 50)
    @Column(name = "action_id")
    private String actionId;
    @Size(max = 50)
    @Column(name = "username")
    private String username;
    @Column(name = "action_date_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date actionDateTime;
    @Lob
    @Size(max = 65535)
    @Column(name = "actions")
    private String actions;

    public UserTrails() {
    }

    public UserTrails(String actionId) {
        this.actionId = actionId;
    }

    public String getActionId() {
        return actionId;
    }

    public void setActionId(String actionId) {
        this.actionId = actionId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getActionDateTime() {
        return actionDateTime;
    }

    public void setActionDateTime(Date actionDateTime) {
        this.actionDateTime = actionDateTime;
    }

    public String getActions() {
        return actions;
    }

    public void setActions(String actions) {
        this.actions = actions;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (actionId != null ? actionId.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof UserTrails)) {
            return false;
        }
        UserTrails other = (UserTrails) object;
        if ((this.actionId == null && other.actionId != null) || (this.actionId != null && !this.actionId.equals(other.actionId))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.pharmaderm.entities.UserTrails[ actionId=" + actionId + " ]";
    }
    
}
