/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.pharmaderm.session;

import com.pharmaderm.entities.Counter;
import com.pharmaderm.entities.CreditDrugs;
import com.pharmaderm.entities.Debtors;
import com.pharmaderm.entities.DrugsReturned;
import com.pharmaderm.entities.PaymentDetails;
import com.pharmaderm.entities.Sales;
import com.pharmaderm.entities.SupplyReturn;
import com.pharmaderm.entities.UserAccount;
import com.pharmaderm.entities.UserTrails;
import java.util.Date;
import java.util.List;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

/**
 *
 * @author Abdul-Rahman Sudais
 */
@Stateless
@LocalBean
@Named
public class PharmadermSessionBean {

    @PersistenceContext(unitName = "Pharmaderm-ejbPU")
    private EntityManager em;

    public void persist(Object object) {
        em.persist(object);
    }

    public UserAccount checkDetails(String username, String password) {
        try {
            return (UserAccount) em.createQuery("SELECT u FROM UserAccount u WHERE u.username =:username AND u.password =:password").setParameter("username", username).setParameter("password", password).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No query!!");
            return null;
        }
    }

    public String userAccountCreate(UserAccount userAccount) {
        try {
            em.persist(userAccount);
            em.flush();
            return userAccount.getUserId();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String salesCreate(Sales sales) {
        try {
            em.persist(sales);
            em.flush();
            return sales.getSalesId();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String CounterCreate(Counter counter) {
        try {
            em.persist(counter);
            em.flush();
            return counter.getCounterId();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<Sales> salesGetAll() {
        try {
            return (List<Sales>) em.createNamedQuery("Sales.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Counter> counterGetAll() {
        try {
            return (List<Counter>) em.createNamedQuery("Counter.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<Sales> findByDayReportType(Date salesDate) {
        try {
            return (List<Sales>) em.createNamedQuery("Sales.findBySalesDate").setParameter("salesDate", salesDate, TemporalType.DATE).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String userTrailsCreate(UserTrails userTrails) {
        try {
            em.persist(userTrails);
            em.flush();
            return userTrails.getActionId();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public List<UserTrails> userTrailsGetAll() {
        try {
            return (List<UserTrails>) em.createNamedQuery("UserTrails.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public Counter findByDrugName(String drugName) {
        try {
            return (Counter) em.createNamedQuery("Counter.findByDrugName").setParameter("drugName", drugName).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public boolean counterUpdate(Counter counter) {
        try {
            em.merge(counter);
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean counterDelete(Counter counter) {
        try {
            em.remove(em.merge(counter));
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean userAccountUpdate(UserAccount userAccount) {
        try {
            em.merge(userAccount);
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public boolean userAccountDelete(UserAccount userAccount) {
        try {
            em.remove(em.merge(userAccount));
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<Sales> findBySingleDrug(String drugName, Date salesDate, Date sales_Date) {
        List<Sales> listOfSaleses = null;
        String qerString = "";
        try {
            qerString = "SELECT s FROM Sales s WHERE s.drugName =:drugName AND s.salesDate BETWEEN :salesDate AND :salesDate";
            listOfSaleses = (List<Sales>) em.createQuery(qerString).setParameter("drugName", drugName).setParameter("salesDate", salesDate, TemporalType.DATE).setParameter("salesDate", sales_Date).getResultList();
            return listOfSaleses;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Not available");
        }
        return null;
    }

    public List<Sales> generalSalesQuery(Date salesDate, Date sales_date) {
        List<Sales> listOfSaleses = null;
        String qString = "";
        try {
            qString = "SELECT s FROM Sales s WHERE s.salesDate BETWEEN :salesDate AND :salesDate";
            listOfSaleses = (List<Sales>) em.createQuery(qString).setParameter("salesDate", salesDate, TemporalType.DATE).setParameter("salesDate", sales_date, TemporalType.DATE).getResultList();
            return listOfSaleses;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("No info available");
        }
        return null;
    }

    public UserAccount findByUsernameInUserAccount(String username) {
        try {
            return (UserAccount) em.createNamedQuery("UserAccount.findByUsername").setParameter("username", username).getSingleResult();
        } catch (Exception e) {
            System.out.println("Not able to");
            e.printStackTrace();
            return null;
        }
    }

    public UserAccount findByPassword(String password) {
        try {
            return (UserAccount) em.createNamedQuery("UserAccount.findByPassword").setParameter("password", password).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<UserAccount> userAccountGetAll() {
        try {
            return (List<UserAccount>) em.createNamedQuery("UserAccount.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String patientsDrugsCreate(DrugsReturned drugsReturned) {
        try {
            em.persist(drugsReturned);
            em.flush();
            return drugsReturned.getReturnedId();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<DrugsReturned> allDrugsForCustomers() {
        try {
            return (List<DrugsReturned>) em.createNamedQuery("DrugsReturned.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String suppliersDrugsCreate(SupplyReturn supplyReturn) {
        try {
            em.persist(supplyReturn);
            em.flush();
            return supplyReturn.getSupId();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<SupplyReturn> supplierisDrugsGetAll() {
        try {
            return (List<SupplyReturn>) em.createNamedQuery("SupplyReturn.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String creditDrugsCreate(CreditDrugs creditDrugs) {
        try {
            em.persist(creditDrugs);
            em.flush();
            return creditDrugs.getDetId();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<CreditDrugs> allDrugsOnCredit() {
        try {
            return (List<CreditDrugs>) em.createNamedQuery("CreditDrugs.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String paymentCreate(PaymentDetails paymentDetails) {
        try {
            em.persist(paymentDetails);
            em.flush();
            return paymentDetails.getPayId();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public List<PaymentDetails> allPaymentDetailsOnDrugs() {
        try {
            return (List<PaymentDetails>) em.createNamedQuery("PaymentDetails.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public List<CreditDrugs> findByCompanies(String companyName, String status){
        try {
            return (List<CreditDrugs>)em.createQuery("SELECT s FROM CreditDrugs s WHERE s.companyName =:companyName AND s.status =:status").setParameter("companyName", companyName).setParameter("status", status).getResultList();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public boolean creditDrugsDelete(CreditDrugs creditDrugs){
        try {
            em.remove(em.merge(creditDrugs));
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public boolean creditDrugsUpdate(CreditDrugs creditDrugs){
        try {
            em.merge(creditDrugs);
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    
    public List<Counter> findByDates(Date dist_date){
        try {
            return (List<Counter>)em.createNamedQuery("Counter.findByDistributionDate").setParameter("distributionDate", dist_date, TemporalType.DATE).getResultList();
        } catch (Exception e) {
            System.out.println("Not found!!");
            e.printStackTrace();
        }
        return null;
    }
    public String createDebtor(Debtors debtors){
        try {
            em.persist(debtors);
            em.flush();
            return debtors.getCompId();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public List<Debtors> debtorsListGetAll(){
        try {
            return (List<Debtors>)em.createNamedQuery("Debtors.findAll").getResultList();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    public boolean debtorUpdate(Debtors debtors){
        try {
            em.merge(debtors);
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public boolean debtorDelete(Debtors debtors){
        try {
            em.remove(em.merge(debtors));
            em.flush();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }
    public Debtors findBedtorsByCompanyName(String companyName){
        try {
            return (Debtors)em.createNamedQuery("Debtors.findByCompanyName").setParameter("companyName", companyName).getSingleResult();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}
